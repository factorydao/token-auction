module.exports = {
  presets: [["es2016"], "react", "@babel/preset-env"],
  plugins: ["babel-plugin-transform-class-properties"],
  transformIgnorePatterns: ["node_modules/(?!evm-chain-scripts/.*)"],
};
