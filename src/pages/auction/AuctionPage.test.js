import React from "react";
import renderer from "react-test-renderer";
import { MemoryRouter } from "react-router-dom";
import AuctionPage from "./AuctionPage";

test("should match snapshot", () => {
  const tree = renderer
    .create(
      <MemoryRouter>
        <AuctionPage />
      </MemoryRouter>
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
