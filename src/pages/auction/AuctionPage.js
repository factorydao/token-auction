import classnames from "classnames";
import Auction from "components/auction";
import StartBlock from "components/contract/start-block/StartBlock";
import FVTTokenLogo from "components/logos/FVTTokenLogo";
import { useGlobalState } from "globalState";
import Page from "pages/Page";
import PropTypes from "prop-types";
import { memo } from "react";
import networks from "utils/networks.json";
import "./AuctionPage.scss";

function AuctionPage({ className }) {
  const [currentNetwork] = useGlobalState("currentNetwork");
  const { availableToken } = networks[currentNetwork];
  return (
    <Page className={classnames("auction-page", className)}>
      <StartBlock>
        <Auction
          assetName={availableToken}
          projectLogo={<FVTTokenLogo />}
          projectUrls={{
            twitter: "https://twitter.com/FactDAO",
            discord: "https://discord.com/invite/factorydao",
          }}
        />
      </StartBlock>
    </Page>
  );
}

AuctionPage.propTypes = {
  className: PropTypes.string,
};

export default memo(AuctionPage);
