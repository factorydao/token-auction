import classnames from "classnames";
import Page from "pages/Page";
import PropTypes from "prop-types";
import { memo } from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Graph from "./graph.svg";
import "./HowItWorksPage.scss";
import Instructions from "./instructions";
import Lot from "./lot.svg";
import Menu from "./menu";
import PriceDecay from "./price-decay.svg";

const HowItWorksPage = ({ className }) => {
  return (
    <Page className={classnames("how-it-works-page", className)}>
      <Container fluid>
        <Row className="how-it-works-page__header align-items-end no-gutters">
          <Col xs={12} md={6}>
            <h1>How it works</h1>
          </Col>
          <Col xs={12} md={6}>
            <Menu />
          </Col>
        </Row>
        <Row className="no-gutters">
          <Col className="how-it-works-page__introduction">
            <p>
              Launch is a decentralised price discovery mechanism, purpose built
              for bootstrapping markets for digital assets.
            </p>
            <p>
              It utilises a new kind of auction, an &quot;exponential token
              auction&quot;, or in the case of a single item a &quot;binary
              search auction&quot;.
            </p>
            <p>
              The system replaces the auctioneer with a smart contract,
              effectively decentralising the auction process.
            </p>
            <p>
              The aims of the system is to find a sensible listing price by
              presenting better than zero slippage buy opportunities at a
              sequence of price ranges.
            </p>
          </Col>
          <Col className="how-it-works-page__graph" md={7}>
            <img className="full-image" src={Graph} alt="" />
            <div>
              <small>
                This is a simulation of an auction using mock data and does not
                reflect actual auction interaction
              </small>
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs={{ span: 12, order: 2 }} md={{ span: "auto", order: 1 }}>
            <img className="full-image" src={Lot} alt="" />
          </Col>
          <Col
            className="d-flex align-items-center justify-content-center"
            xs={{ order: 1 }}
            md={{ order: 2, offset: 1 }}
          >
            <div className="tokens-description">
              <h2>
                There are 20m tokens <br /> to be distributed
              </h2>
              <p>
                The auction works in lots. <br /> When the first lot of tokens
                is sold out, the supply doubles along with the price. Supply and
                price double in every lot. <br />
                If there are no buyers the price will fall.
              </p>
            </div>
          </Col>
        </Row>
        <Row>
          <Col className="d-flex flex-column justify-content-center">
            <h2 className="text-secondary">Price decay mechanism</h2>
            <p className="price-decay-description">
              This rapid upward bias is countered with a rapid downward bias.
              Every block that goes by from the lot opening lowers the price.
              Price moves up quickly, and down quickly.
            </p>
          </Col>
          <Col className="how-it-works-page__price-decay" md={7}>
            <img className="full-image" src={PriceDecay} alt="" />
          </Col>
        </Row>
        <Row>
          <Col>
            <Instructions />
          </Col>
        </Row>
        <Row>
          <Col className="how-it-works-page__instructions2">
            <p>
              If there <strong>are</strong> enough tokens available in the lot
              you will obtain your desired allocation of tokens.
              <br />
              If there <strong>aren&apos;t</strong>, you will either receive
              whatever is left in the lot and be refunded the difference,
              <br />
              or if you have missed the lot your transaction will fail.
            </p>
            <p>When all tokens are sold the auction is over.</p>
            <div className="tokens-collected-header">
              <h2>
                All tokens collected will be used to bootstap a liquidity pool
              </h2>
            </div>
            <Menu />
          </Col>
        </Row>
      </Container>
    </Page>
  );
};

HowItWorksPage.propTypes = {
  className: PropTypes.string,
};

export default memo(HowItWorksPage);
