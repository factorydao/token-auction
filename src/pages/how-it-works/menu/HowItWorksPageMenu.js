import classnames from "classnames";
import PropTypes from "prop-types";
import { memo } from "react";
import { Link } from "react-router-dom";
import "./HowItWorksPageMenu.scss";

function HowItWorksPageMenu({ className }) {
  return (
    <div className={classnames("how-it-works-page-menu", className)}>
      <Link to="/">
        <p
          className="btn btn-outline-primary"
          style={{ background: "#FDF446", color: "#000000" }}
        >
          to the auction
        </p>
      </Link>
    </div>
  );
}

HowItWorksPageMenu.propTypes = {
  className: PropTypes.string,
};

export default memo(HowItWorksPageMenu);
