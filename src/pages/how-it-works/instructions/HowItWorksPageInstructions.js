import classnames from "classnames";
import PropTypes from "prop-types";
import { Fragment, memo } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Ethereum from "./ethereum.svg";
import Form from "./form.svg";
import "./HowItWorksPageInstructions.scss";
import MetaMask from "./metamask.svg";

function Item({ col1, col2 }) {
  return (
    <Container className="how-it-works-page-instructions__item">
      <Row>
        <Col xs={12}>{col1}</Col>
        <Col xs={12}>{col2}</Col>
      </Row>
    </Container>
  );
}

function HowItWorksPageInstructions({ className }) {
  return (
    <div className={classnames("how-it-works-page-instructions", className)}>
      <Item
        col1={
          <div className="how-it-works-page-instructions__icons-container">
            <img src={MetaMask} alt="MetMask" />
            <img src={Ethereum} alt="Ethereum" />
          </div>
        }
        col2={
          <Fragment>
            You will need a compatible wallet and the payment token to
            participate
          </Fragment>
        }
      />
      <Item
        col1={<img src={Form} alt="" />}
        col2={<Fragment>Enter the amount you are willing to spend</Fragment>}
      />
      <Item
        col1={
          <Button className="buy-button" variant="primary">
            Buy
          </Button>
        }
        col2={
          <Fragment>
            Hit &quot;BUY&quot; if you want
            <br />
            to buy, don&apos;t if you
            <br />
            don&apos;t
          </Fragment>
        }
      />
    </div>
  );
}

HowItWorksPageInstructions.propTypes = {
  className: PropTypes.string,
};

export default memo(HowItWorksPageInstructions);
