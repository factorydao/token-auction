import { get } from "axios";

export const getIPInfo = () => {
  const controller = new AbortController(),
    deferred = get("https://pro.ip-api.com/json?key=Fvzq8t5Z8NUskwP", {
      signal: controller.signal,
    });

  deferred.abort();

  return deferred;
};
