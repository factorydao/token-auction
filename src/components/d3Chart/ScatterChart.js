import classnames from "classnames";
import {
  axisBottom,
  axisLeft,
  extent,
  max,
  scaleLinear,
  zoomIdentity,
} from "d3";
import { select } from "d3-selection";
import { zoom } from "d3-zoom";
import { useEffect, useRef, useState } from "react";
import { isMobile } from "react-device-detect";
import Axis from "./Axis/Axis";
import styles from "./Chart.module.scss";
import { onScroll } from "./helpers";
import Legend from "./Legend/Legend";
import Rectangles from "./Rectangles";

const desktopMargin = { top: 0, right: 15, bottom: 60, left: 110 };
const mobileMargin = { top: 10, right: 15, bottom: 30, left: 80 };

/**
 *
 * @data []{
 *"trancheNumber": number,
 *"tokensLeftInTranche": float number,
 *"isEmptyTranche": boolean,
 *"volume": number,
 *"tooltip": {
 *     "timestamp": number,
 *      "trancheNumber": number,
 *       "amount": string,
 *    "price": wei String,
 *     "cost": string
 *  },
 *   "x": number,
 *    "y": number,
 *}
 * @marks []{ value: 0.0001 formatted value, label: string, y: number }
 * @returns Scatter Chart
 */
const ScatterChart = ({
  data,
  marks,
  height,
  width,
  blockWithSpacing = 12,
}) => {
  const margin = isMobile ? mobileMargin : desktopMargin;
  const offsetRatio = isMobile ? 1.8 : 1.2;
  const legendOffsetRatio = isMobile ? 0.35 : 0.65;
  const yRef = useRef();
  const xRef = useRef();
  const containerRef = useRef(null);
  const scrollGroupRef = useRef(null);
  const [xPan, setXPan] = useState(0);
  const [{ x, y }, setScales] = useState({});

  useEffect(() => {
    if (!marks.length) return;
    const xScale = scaleLinear()
      .domain([0, max(data, (item) => item.x)])
      .range([0, data.length * blockWithSpacing]);
    const yScale = scaleLinear()
      .domain(extent(marks, (item) => item.y))
      .range([height, -blockWithSpacing]);
    setScales({
      x: xScale,
      y: yScale,
    });
  }, [width, height, marks.length]);

  useEffect(() => {
    if (!scrollGroupRef.current || !data?.length) return;
    const scrollGroup = select(scrollGroupRef.current);
    const container = containerRef.current;
    const { width: containerWidth } = container.getBoundingClientRect();
    const initialOffSetCalc =
      data.length * blockWithSpacing - containerWidth / offsetRatio;
    const initialOffset = initialOffSetCalc > 0 ? `-${initialOffSetCalc}` : 0;
    const d3Zoom = zoom()
      .scaleExtent([1, 1])
      .on("zoom", () =>
        onScroll(
          scrollGroup,
          initialOffset,
          margin.top + blockWithSpacing,
          setXPan
        )
      );
    select(container)
      .call(d3Zoom)
      .on("wheel.zoom", () => {
        onScroll(
          scrollGroup,
          initialOffset,
          margin.top + blockWithSpacing,
          setXPan
        );
      });

    select(container).call(d3Zoom.transform, zoomIdentity);
    scrollGroup.attr(
      "transform",
      `translate(${initialOffset} ,-${margin.top + blockWithSpacing})`
    );
  }, [scrollGroupRef.current, data?.length]);

  return (
    <div ref={containerRef} style={{ width: "100%", height: "100%" }}>
      <svg
        width={"100%"}
        height={"100%"}
        className={classnames("chart", styles.svg)}
      >
        <g
          transform={`translate(${margin.left},${margin.top})`}
          width={width}
          height={height}
        >
          <Rectangles
            data={data}
            scale={{ x, y }}
            rangeX={width}
            rangeY={height}
            ref={scrollGroupRef}
            xPan={xPan}
            blockHeightWithPadding={blockWithSpacing - 2}
          />
          {x?.range ? (
            <>
              <Legend x={width * legendOffsetRatio} y={0} />
              <Axis
                ref={xRef}
                transform={"translate(0," + (height - margin.top - 1) + ")"}
                scale={axisBottom().scale(x).tickValues([]).tickSizeOuter(0)}
              />
              <g className={styles.xAxisProlongate}>
                <rect
                  fill="white"
                  height="2px"
                  width={width}
                  transform={"translate(0," + (height - margin.top - 1) + ")"}
                />
              </g>
              <g className={styles.curtain}>
                <rect
                  x={-margin.left}
                  y={0}
                  height="100%"
                  width={margin.left}
                />
              </g>
              <Axis
                ref={yRef}
                transform={`translate(0,-${margin.top})`}
                scale={axisLeft()
                  .scale(y)
                  .tickValues(marks.map((m) => m.y))
                  .tickFormat((value) => (value ? marks[value].label : null))
                  .tickSizeOuter(0)}
                yScale
              />
            </>
          ) : null}
        </g>
      </svg>
    </div>
  );
};
export default ScatterChart;
