import { zoomTransform } from "d3-zoom";

export function sortObjects(objArray, properties) {
  var primers = arguments[2] || {};

  properties = properties.split(/\s*,\s*/).map(function (prop) {
    prop = prop.match(/^([^\s]+)(\s*desc)?/i);
    if (prop[2] && prop[2].toLowerCase() === "desc") {
      return [prop[1], -1];
    } else {
      return [prop[1], 1];
    }
  });

  function valueCmp(x, y) {
    if (x > y) return 1;
    return x < y ? -1 : 0;
  }

  function arrayCmp(a, b) {
    var arr1 = [],
      arr2 = [];
    properties.forEach(function (prop) {
      var aValue = a[prop[0]],
        bValue = b[prop[0]];
      if (typeof primers[prop[0]] != "undefined") {
        aValue = primers[prop[0]](aValue);
        bValue = primers[prop[0]](bValue);
      }
      arr1.push(prop[1] * valueCmp(aValue, bValue));
      arr2.push(prop[1] * valueCmp(bValue, aValue));
    });
    return arr1 < arr2 ? -1 : 1;
  }

  return objArray.sort(function (a, b) {
    return arrayCmp(a, b);
  });
}
export const onScroll = (scrollGroup, initialOffset, marginTop, setXPan) => {
  const transform = zoomTransform(scrollGroup.node());
  setXPan(Math.trunc(transform.x + Number(initialOffset)));
  const translateX = Number(initialOffset) + transform.x;
  scrollGroup.attr("transform", `translate( ${translateX} , -${marginTop})`);
};
export const BLOCK_TYPES = {
  OPEN: "open",
  EMPTY: "empty",
  CLOSED: "closed",
  ACTIVE: "active",
};

const sampleObject = {
  price: {
    unquantized: "0.0000999999999927",
    tokensPerEth: "10000",
  },
  ethSpent: "0.1",
  tokensAcquired: "1000.000000073000000005",
  tokensLeftInTranche: "8999.999999926999999995",
  trancheNumber: 1,
  purchaser: "0x288fE43139741F91a8Cbb6F4adD83811c794851b",
  txHash: "0x820d2032cd1e6587028c14d17267b060ad6b280ac6dc3d01e9e6fbe84b769656",
  timestamp: 1646122986,
};

export const generateRandomEvents = (amount, maxPrice) => {
  const result = [
    {
      price: {
        unquantized: "0.0500999999999927",
        tokensPerEth: "10000",
      },
      ethSpent: "0.5",
      tokensAcquired: "1000.000000073000000005",
      tokensLeftInTranche: "8999.999999926999999995",
      trancheNumber: 1,
      purchaser: "0x288fE43139741F91a8Cbb6F4adD83811c794851b",
      txHash:
        "0x820d2032cd1e6587028c14d17267b060ad6b280ac6dc3d01e9e6fbe84b769656",
      timestamp: 1646122986,
    },
  ];
  for (let i = 0; i < amount; i++) {
    const _obj = sampleObject;
    const calcPrice = Math.random() * maxPrice;
    _obj.price.unquantized = String(calcPrice);
    result.push(_obj);
  }
  return result;
};

/**
 * PART OF TOUCH EVENTS FOR SCATTER CHART
 */

//  select(container)
//  .call(d3Zoom)
// .on("touchstart", onTouchStart, true)
// .on("touchmove", onTouchMove, true)
// .on(
//   "touchend",
//   () => {
//     onTouchEnd(d3Zoom);
//   },
//   true
// )
//  .on("wheel.zoom", () => {
//    onScroll(scrollGroup, initialOffset, margin.top, setXPan);
//  });

// const getTouchObject = ({ changedTouches }) => changedTouches[0];

// console.log(startXRef.current);
// const scrollXDisabled = useRef(false);
// const startXRef = useRef(0);
// const startYRef = useRef(0);
// const isXPanRef = useRef(false);
// const isYPanRef = useRef(false);
// const onTouchStart = (event) => {
//   const touch = getTouchObject(event);
//   // startXRef.current = touch.pageX;
//   // startYRef.current = touch.pageY;
// };

// const onTouchMove = (event) => {
//   // const touch = getTouchObject(event);
//   // const diffX = startXRef.current - touch.pageX;
//   // const diffY = startYRef.current - touch.pageY;

//   // if (diffX >= 10 || diffX <= -10) isXPanRef.current = true;
//   // if (diffY >= 3 || diffY <= -3) isYPanRef.current = true;

//   if (!isXPanRef.current && isYPanRef.current && !scrollXDisabled.current) {
//     select(containerRef.current).on(".zoom", null);
//     scrollXDisabled.current = true;
//   }
//   // if (scrollXDisabled) window.scrollBy(0, diffY);
// };

// const onTouchEnd = (zoomBehavior) => {
//   select(containerRef.current).call(zoomBehavior);
//   scrollXDisabled.current = false;
//   isXPanRef.current = false;
//   isYPanRef.current = false;
// };
