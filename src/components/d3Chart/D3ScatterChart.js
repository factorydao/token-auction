import classnames from "classnames";
import Spinner from "components/indicators/spinner/Spinner";
import { useGlobalState } from "globalState";
import { useEventsV2, useInitialPrice } from "queries/contract";
import { useEffect, useRef, useState } from "react";
import { formatNumber } from "utils/formats";
import networks from "utils/networks.json";
import { sortObjects } from "./helpers";
import ScatterChart from "./ScatterChart";

const labelFormat = "0,0.000000";
const markSpacing = 53;

const mapBlockY = (arr, numToFind) => {
  const _arr = arr.map((elem) => elem.value);
  const lowerNumber = _arr.reverse().find((elem) => elem <= numToFind);
  const { y } = arr.find((elem) => elem.value === lowerNumber);
  const result = y + numToFind / lowerNumber / 2;
  return isFinite(result) ? result : y ?? 0;
};

const D3ScatterChart = ({ onError, className }) => {
  const [currentNetwork] = useGlobalState("currentNetwork");
  const tokenSymbol = networks[currentNetwork].symbol;
  const containerRef = useRef();
  const [containerHeight, setHeight] = useState(600);
  const [containerWidth, setWidth] = useState(800);
  // const { isLoading, error, events } = useEvents();
  const { isLoading, error, data: events } = useEventsV2();
  const { data: initialPrice } = useInitialPrice();
  const [data, setData] = useState();
  const [marks, setMarks] = useState([]);
  const [markCount, setMarkCount] = useState(containerHeight / markSpacing);

  //wait for resize of container and set dimensions
  useEffect(() => {
    const { height, width } = containerRef.current.getBoundingClientRect();
    setMarkCount(height / markSpacing);
    setWidth(width);
    setHeight(height);
  }, [
    containerRef.current?.clientHeight,
    containerRef.current?.clientWidth,
    marks.length,
  ]);

  useEffect(() => {
    if (isLoading || !events?.length || !initialPrice) return;
    const _marksArr = [{ value: 0, label: `0 ${tokenSymbol}`, y: 0 }];

    for (let i = 0; i < markCount; i++) {
      const value = 2 ** i * initialPrice;
      const label = formatNumber(value, labelFormat) + ` ${tokenSymbol}`;
      _marksArr.push({ value: value, label, y: i + 1 });
    }
    const nonEmptyEvents = events?.filter((x) => parseFloat(x.ethSpent) > 0);
    const data = sortObjects(
      nonEmptyEvents.map((event) => {
        const { price, timestamp, trancheNumber, tokensAcquired, ethSpent } =
          event;
        const unquantizedPrice = parseFloat(price.unquantized);
        const tokensLeftInTranche = parseFloat(event.tokensLeftInTranche);
        const formatted = formatNumber(unquantizedPrice, labelFormat);
        const idx = _marksArr.findIndex(
          (item) => item.value === Number(formatted)
        );
        const mark = _marksArr[idx];
        const y = mark?.y ?? mapBlockY(_marksArr, formatted);

        return {
          trancheNumber,
          tokensLeftInTranche,
          isEmptyTranche: tokensLeftInTranche <= 0,
          volume: parseInt(tokensAcquired),
          tooltip: {
            timestamp: timestamp * 1000,
            trancheNumber: trancheNumber,
            amount: tokensAcquired,
            price: price.unquantized,
            cost: ethSpent,
            symbol: tokenSymbol,
          },
          x: parseInt(timestamp),
          y,
          yLabel: unquantizedPrice,
        };
      }),
      "x, trancheNumber, isEmptyTranche"
    ).map((event, index) => ({
      ...event,
      x: index,
    }));
    setData(data);
    setMarks(_marksArr);
  }, [events]);
  useEffect(() => {
    onError && onError(error);
  }, [error, onError]);

  return (
    <div className={classnames("auction-grid", className)} ref={containerRef}>
      {isLoading ? (
        <Spinner />
      ) : (
        <ScatterChart
          width={containerWidth}
          height={containerHeight}
          data={data}
          marks={marks}
        />
      )}
    </div>
  );
};
export default D3ScatterChart;
