import classnames from "classnames";
import moment from "moment";
import { memo } from "react";
import { formatNumber } from "utils/formats";
import styles from "./Tooltip.module.scss";

const Tooltip = ({
  className,
  x = 0,
  y = 0,
  height = "100%",
  width = "100%",
  timestamp,
  amount,
  price,
  cost,
  trancheNumber,
  symbol,
}) => {
  return (
    <foreignObject
      className={classnames(styles.tooltip, className)}
      x={x}
      y={y}
      width={width}
      height={height}
    >
      <div className={styles.content}>
        <span className={styles.timestamp}>
          {moment(timestamp).format("HH:mm:ss L")}
        </span>
        <br />
        Tranche: {trancheNumber}
        <br />
        Amount: {formatNumber(amount)}
        <br />
        Price: {formatNumber(price)} {symbol || "ETH"}
        <br />
        Cost: {formatNumber(cost)} {symbol || "ETH"}
      </div>
    </foreignObject>
  );
};

export default memo(
  Tooltip,
  (prev, next) => JSON.stringify(prev) === JSON.stringify(next)
);
