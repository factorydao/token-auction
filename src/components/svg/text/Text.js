import classnames from "classnames";
import PropTypes from "prop-types";
import { forwardRef, memo } from "react";
import "./Text.scss";

export const LENGTH_ADJUST = {
  SPACING: "spacing",
  SPACING_AND_GLYPHS: "spacingAndGlyphs",
};

const Text = forwardRef((props, ref) => {
  return (
    <text
      ref={ref}
      {...props}
      className={classnames("text", props.className)}
    />
  );
});
Text.displayName = "Text";
Text.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  dx: PropTypes.number,
  dy: PropTypes.number,
  rotate: PropTypes.number,
  lengthAdjust: PropTypes.oneOf(Object.values(LENGTH_ADJUST)),
  textLength: PropTypes.number,
};

Text.defaultProps = {
  x: 0,
  y: 0,
  lengthAdjust: LENGTH_ADJUST.SPACING,
};

export default memo(Text);
