import React from "react";
import renderer from "react-test-renderer";
import SVG from "./SVG";

test("should match snapshot", () => {
  const tree = renderer.create(<SVG />).toJSON();

  expect(tree).toMatchSnapshot();
});
