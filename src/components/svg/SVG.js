import assert from "assert";
import classnames from "classnames";
import PropTypes from "prop-types";
import { memo, useCallback, useEffect, useRef, useState } from "react";
import ErrorBoundry from "utils/ErrorBoundry";
import usePan from "utils/movable/usePan";
import useScroll from "utils/movable/useScroll";
import "./SVG.scss";

const SVG = (props) => {
  const {
      className,
      movable,
      scrollable,
      width,
      height,
      moveX,
      moveY,
      onMove,
    } = props,
    svgProps = { ...props },
    ref = useRef(null),
    [viewBox, setViewBox] = useState(props.viewBox || [0, 0, width, height]),
    getSvgBox = () => ref.current.getBBox(),
    [setPanElement] = usePan(move),
    [setScrollableElement] = useScroll(move),
    refreshViewBox = useCallback(
      (x, y) => {
        assert(!isNaN(x), `x is not a number, ${x}`);
        assert(!isNaN(y), `y is not a number, ${y}`);

        const svgBox = getSvgBox(),
          width = movable ? viewBox[2] : svgBox.width,
          height = movable ? viewBox[3] : svgBox.height;

        setViewBox([x, y, width, height]);

        onMove && onMove({ x, y, width, height });
      },
      [viewBox, movable, onMove]
    );

  function move(offsetX, offsetY) {
    const x = moveX ? viewBox[0] - offsetX : viewBox[0],
      y = moveY ? viewBox[1] - offsetY : viewBox[1],
      bounds = keepWithinBounds(getSvgBox(), { x, y });

    refreshViewBox(bounds.x, bounds.y);
  }

  function keepWithinBounds(bounds, target) {
    return target;
    // assert(bounds, `bounds is invalid, ${bounds}`)
    // assert(!isNaN(bounds.x), `bounds.x is not a number, ${bounds.x}`)
    // assert(!isNaN(bounds.y), `bounds.y is not a number, ${bounds.y}`)

    // return {
    //   x: Math.min(Math.max(Math.min(0, bounds.x), target.x), (bounds.x + bounds.width - width)),
    //   y: Math.min(Math.max(Math.min(0, bounds.y), target.y), (bounds.y + bounds.height - height))
    // }
  }

  useEffect(() => {
    setViewBox((viewBox) => [viewBox[0], viewBox[1], width, height]);
  }, [width, height]);

  useEffect(() => {
    if (movable) {
      setPanElement(ref.current);
    }
  }, [ref, movable, setPanElement]);

  useEffect(() => {
    if (scrollable) {
      setScrollableElement(ref.current);
    }
  }, [ref, scrollable, setScrollableElement]);

  useEffect(() => {
    function handleResize() {
      if (viewBox) {
        refreshViewBox(viewBox[0], viewBox[1]);
      }
    }

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  delete svgProps.scrollable;
  delete svgProps.moveX;
  delete svgProps.moveY;
  delete svgProps.movable;
  delete svgProps.onMove;

  return (
    <ErrorBoundry>
      <svg
        ref={ref}
        xmlns="http://www.w3.org/2000/svg"
        {...svgProps}
        className={classnames("svg", { "svg--movable": movable }, className)}
        viewBox={viewBox && viewBox.join(" ")}
      />
    </ErrorBoundry>
  );
};

SVG.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  movable: PropTypes.bool,
  scrollable: PropTypes.bool,
  moveX: PropTypes.bool,
  moveY: PropTypes.bool,
  onMove: PropTypes.func,
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  preserveAspectRatio: PropTypes.oneOfType([
    PropTypes.oneOf([
      "xMinYMin",
      "xMidYMin",
      "xMaxYMin",
      "xMinYMid",
      "xMidYMid",
      "xMaxYMid",
      "xMinYMax",
      "xMidYMax",
      "xMaxYMax",
    ]),
  ]),
  viewBox: PropTypes.arrayOf(PropTypes.number),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

SVG.defaultProps = {
  movable: false,
  moveX: true,
  moveY: true,
  scrollable: false,
  preserveAspectRatio: "xMinYMin",
  x: 0,
  y: 0,
  width: 100,
  height: 100,
};

export default memo(SVG);
