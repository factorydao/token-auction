import PropTypes from "prop-types";
import { forwardRef, memo } from "react";

const Rect = forwardRef(
  ({ className, x, y, width, height, rx, ry, pathLength }, ref) => {
    return (
      <rect
        ref={ref}
        className={className}
        x={x}
        y={y}
        width={width}
        height={height}
        rx={rx}
        ry={ry}
        pathLength={pathLength}
      />
    );
  }
);
Rect.displayName = "Rect";
Rect.propTypes = {
  className: PropTypes.string,
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  rx: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  ry: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  pathLength: PropTypes.number,
};

Rect.defaultProps = {
  x: 0,
  y: 0,
};

export default memo(Rect);
