import React from "react";
import renderer from "react-test-renderer";
import Rect from "./Rect";

test("should match snapshot", () => {
  const tree = renderer.create(<Rect />).toJSON();

  expect(tree).toMatchSnapshot();
});
