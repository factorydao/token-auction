import classnames from "classnames";
import PropTypes from "prop-types";
import { forwardRef, memo } from "react";
import "./Line.scss";

const Line = forwardRef(({ className, x1, x2, y1, y2, pathLength }, ref) => {
  return (
    <line
      ref={ref}
      className={classnames("line", className)}
      x1={x1}
      x2={x2}
      y1={y1}
      y2={y2}
      pathLength={pathLength}
    />
  );
});
Line.displayName = "Line";

Line.propTypes = {
  className: PropTypes.string,
  x1: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  x2: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y1: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y2: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  pathLength: PropTypes.number,
};

Line.defaultProps = {
  x: 0,
  y: 0,
};

export default memo(Line);
