import React from "react";
import renderer from "react-test-renderer";
import HookedTokenDetailsBox from "./HookedTokenDetailsBox";

function useHook() {
  return { isLoading: false, error: null, data: 1 };
}

test("should match snapshot", () => {
  const tree = renderer
    .create(
      <HookedTokenDetailsBox
        label="Tokens available"
        hook={useHook()}
        suffix="ETH"
      />
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
