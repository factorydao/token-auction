import React, { memo } from "react";
import PropTypes from "prop-types";
import Spinner from "components/indicators/spinner";
import TokenDetailsBox from "./TokenDetailsBox";

function HookedTokenDetailsBox({ className, label, hook, valueFormat }) {
  const { isLoading, error, data } = hook;

  if (error) {
    console.error(error);
  }

  if (isLoading) {
    return <Spinner className={className} />;
  }

  return (
    <TokenDetailsBox
      className={className}
      label={label}
      value={parseFloat(data)}
      valueFormat={valueFormat}
    />
  );
}

HookedTokenDetailsBox.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  hook: PropTypes.object.isRequired,
  valueFormat: PropTypes.string,
  suffix: PropTypes.string,
};

export default memo(HookedTokenDetailsBox);
