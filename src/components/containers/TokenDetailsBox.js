import React, { memo } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { FormattedNumber } from "utils/formats";
import "./TokenDetailsBox.scss";

function TokenDetailsBox({ className, label, value, valueFormat, suffix }) {
  return (
    <div className={classnames("token-details-box", className)}>
      <div className="token-details-box__value">
        <FormattedNumber number={value} format={valueFormat} suffix={suffix} />
      </div>
      <div className="token-details-box__label">{label}</div>
    </div>
  );
}

TokenDetailsBox.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  valueFormat: PropTypes.string,
  suffix: PropTypes.string,
};

export default memo(TokenDetailsBox);
