import React from "react";
import renderer from "react-test-renderer";
import AuctionTable from "./AuctionTable";

test("should match snapshot", () => {
  const tree = renderer.create(<AuctionTable />).toJSON();

  expect(tree).toMatchSnapshot();
});
