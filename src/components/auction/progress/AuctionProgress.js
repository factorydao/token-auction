import Spinner from "components/indicators/spinner";
import { useTotalTokensOffered, useTotalTokensSold } from "queries/contract";
import React, { memo, useEffect, useState } from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import { formatNumber, FormattedNumber } from "utils/formats";
import "./AuctionProgress.scss";

function AuctionProgress() {
  const [now, setNow] = useState(0),
    [max, setMax] = useState(0),
    [isLoading, setIsLoading] = useState(0),
    totalTokensSold = useTotalTokensSold(),
    totalTokensOffered = useTotalTokensOffered();

  useEffect(() => {
    if (totalTokensSold.error) {
      console.error(totalTokensSold.error);
    }

    if (totalTokensOffered.error) {
      console.error(totalTokensOffered.error);
    }

    setIsLoading(totalTokensSold.isLoading || totalTokensOffered.isLoading);
    setNow(parseFloat(totalTokensSold.data) || 0);
    setMax(parseFloat(totalTokensOffered.data) || 0);
  }, [
    totalTokensSold.isLoading,
    totalTokensSold.error,
    totalTokensSold.data,
    totalTokensOffered.isLoading,
    totalTokensOffered.error,
    totalTokensOffered.data,
  ]);

  return (
    <div className="auction-progress">
      {isLoading ? (
        <Spinner size="sm" />
      ) : (
        <div className="text-right">
          <ProgressBar
            className="auction-progress__progress-bar"
            min={0}
            max={max}
            now={max - now}
            label={`Available supply ${formatNumber(max - now)}`}
          />
          <div className="auction-progress__label">
            <span>0</span>
            <FormattedNumber number={max} />
          </div>
        </div>
      )}
    </div>
  );
}

export default memo(AuctionProgress);
