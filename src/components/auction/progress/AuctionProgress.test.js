import React from "react";
import renderer from "react-test-renderer";
import AuctionProgress from "./AuctionProgress";

test("should match snapshot", () => {
  const tree = renderer
    .create(<AuctionProgress max={20000000} now={9000000} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
