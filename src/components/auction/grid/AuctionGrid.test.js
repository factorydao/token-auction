import React from "react";
import renderer from "react-test-renderer";
import AuctionGrid from "./AuctionGrid";

test("should match snapshot", () => {
  const tree = renderer.create(<AuctionGrid />).toJSON();

  expect(tree).toMatchSnapshot();
});
