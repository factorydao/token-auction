import classnames from "classnames";
import Grid from "components/graph/grid";
import PropTypes from "prop-types";
import { useEventsV2, useInitialPrice } from "queries/contract";
import { memo, useEffect, useMemo } from "react";
import "./AuctionGrid.scss";

function sortObjects(objArray, properties /*, primers*/) {
  var primers = arguments[2] || {};

  properties = properties.split(/\s*,\s*/).map(function (prop) {
    prop = prop.match(/^([^\s]+)(\s*desc)?/i);
    if (prop[2] && prop[2].toLowerCase() === "desc") {
      return [prop[1], -1];
    } else {
      return [prop[1], 1];
    }
  });

  function valueCmp(x, y) {
    if (x > y) return 1;
    return x < y ? -1 : 0;
  }

  function arrayCmp(a, b) {
    var arr1 = [],
      arr2 = [];
    properties.forEach(function (prop) {
      var aValue = a[prop[0]],
        bValue = b[prop[0]];
      if (typeof primers[prop[0]] != "undefined") {
        aValue = primers[prop[0]](aValue);
        bValue = primers[prop[0]](bValue);
      }
      arr1.push(prop[1] * valueCmp(aValue, bValue));
      arr2.push(prop[1] * valueCmp(bValue, aValue));
    });
    return arr1 < arr2 ? -1 : 1;
  }

  return objArray.sort(function (a, b) {
    return arrayCmp(a, b);
  });
}

function AuctionGrid({ className, height, width, onLoading, onError }) {
  // const { isLoading, error, events } = useEvents(),
  const { isLoading, error, data: events } = useEventsV2(),
    initialPrice = useInitialPrice(),
    data = useMemo(() => {
      if (isLoading) {
        return [];
      }

      return (
        events &&
        sortObjects(
          events
            .filter((x) => parseFloat(x.ethSpent) > 0)
            .map((event) => {
              const {
                  price,
                  timestamp,
                  trancheNumber,
                  tokensAcquired,
                  ethSpent,
                } = event,
                unquantizedPrice = parseFloat(price.unquantized),
                tokensLeftInTranche = parseFloat(event.tokensLeftInTranche);

              return {
                trancheNumber,
                tokensLeftInTranche,
                isEmptyTranche: tokensLeftInTranche <= 0,
                volume: parseInt(tokensAcquired),
                tooltip: {
                  timestamp: timestamp * 1000,
                  trancheNumber: trancheNumber,
                  amount: tokensAcquired,
                  price: price.unquantized,
                  cost: ethSpent,
                },
                x: parseInt(timestamp),
                y: Math.floor(
                  Math.log2(unquantizedPrice * 100000000) / Math.log2(1.2)
                ),
                yLabel: unquantizedPrice,
              };
            }),
          "x, trancheNumber, isEmptyTranche"
        )
          // .sort((a, b) => {
          //   const x1 = a.x
          //       , x2 = b.x

          //   if (x1 < x2) {
          //     return -1
          //   }

          //   if (x1 > x2) {
          //     return 1
          //   }

          //   return 0
          // })
          .map((event, index) => ({
            ...event,
            x: index,
          }))
      );
    }, [isLoading, events]),
    rangeX = useMemo(
      () => ({
        min: data[0] ? data[0].x : 0,
        max: data.length > 0 ? data[data.length - 1].x : 0,
      }),
      [data]
    ),
    rangeY = useMemo(
      () =>
        data.reduce(
          ({ min, max }, currentValue) => ({
            min: Math.min(currentValue.y, min),
            max: Math.max(currentValue.y, max),
          }),
          {
            min: data[0] ? data[0].y : 0,
            max: data[0] ? data[0].y : 0,
          }
        ),
      [data]
    ),
    rangeYLabel = useMemo(
      () =>
        data.reduce(
          ({ min, max }, currentValue) => ({
            min: Math.min(currentValue.yLabel, min),
            max: Math.max(currentValue.yLabel, max),
          }),
          {
            min: data[0] ? data[0].yLabel : 0,
            max: data[0] ? data[0].yLabel : 0,
          }
        ),
      [data]
    );

  useEffect(() => {
    onLoading && onLoading(isLoading);
  }, [isLoading, onLoading]);

  useEffect(() => {
    onError && onError(error);
  }, [error, onError]);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  return (
    <Grid
      className={classnames("auction-grid", className)}
      minX={rangeX.min}
      maxX={rangeX.max}
      minY={rangeY.min}
      maxY={rangeY.max}
      minYLabel={rangeYLabel.min}
      maxYLabel={rangeYLabel.max}
      initialValue={parseFloat(initialPrice.data) || 0}
      blockSpacing={3}
      height={height}
      width={width}
      data={data}
      yAxisMarkSuffix="ETH"
      showNextBlock={true}
    />
  );
}

AuctionGrid.propTypes = {
  className: PropTypes.string,
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onLoading: PropTypes.func,
  onError: PropTypes.func,
};

export default memo(AuctionGrid);
