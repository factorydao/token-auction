import classnames from "classnames";
import PropTypes from "prop-types";
import { memo } from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import "./AssetProfile.scss";

function AssetProfile({ className, name, logo, urls }) {
  return (
    <Container className={classnames("asset-profile", className)} fluid>
      <Row className="align-items-center">
        <Col xs="auto" className="align-self-start">
          {logo}
        </Col>
        <Col>
          <Row xs={1}>
            <Col className="asset-profile__name">{name}</Col>
            {urls && (
              <Col className="asset-profile__urls align-self-end">
                {Object.keys(urls).map((key) => (
                  <a
                    key={`asset-profile-${key}-url`}
                    className="asset-profile__url"
                    href={urls[key]}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {key}
                  </a>
                ))}
              </Col>
            )}
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

AssetProfile.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  logo: PropTypes.element.isRequired,
  urls: PropTypes.shape({
    website: PropTypes.string,
    memo: PropTypes.string,
    telegram: PropTypes.string,
    twitter: PropTypes.string,
  }),
};

export default memo(AssetProfile);
