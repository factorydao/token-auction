import FVTTokenLogo from "components/logos/FVTTokenLogo";
import renderer from "react-test-renderer";
import AssetProfile from "./AssetProfile";

test("should match snapshot", () => {
  const tree = renderer
    .create(
      <AssetProfile
        name="FVT"
        logo={<FVTTokenLogo />}
        urls={{
          website: "https://finance.vote",
          medium: "https://medium.com/@financedotvote",
          telegram: "https://t.me/financedotvote",
          twitter: "https://twitter.com/financedotvote",
        }}
      />
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
