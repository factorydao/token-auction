import classnames from "classnames";
import PropTypes from "prop-types";
import { memo, useState } from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import ErrorBoundry from "utils/ErrorBoundry";
import "./Auction.scss";
import Form from "./form";
import Header from "./header";
import NewChartPage from "./NewChart/NewChartPage";
import Progress from "./progress";
import Table from "./table";
function Auction({ assetName, projectLogo, projectUrls }) {
  const [gridError, setGridError] = useState(false);

  function handleGridError(err) {
    setGridError(err);
  }

  return (
    <Container className="auction" fluid>
      <Row>
        <Col>
          <Header
            className="auction__header"
            assetName={assetName}
            projectLogo={projectLogo}
            projectUrls={projectUrls}
          />
          <Progress />
        </Col>
      </Row>
      <ErrorBoundry>
        <Row>
          <Col xs={12} md={7}>
            <div
              className={classnames("auction__grid-wrapper", {
                "auction__grid-wrapper--error": gridError,
              })}
            >
              <NewChartPage onError={handleGridError} />
            </div>
          </Col>
          <Col xs={12} md={5}>
            <Form
              className="auction__form"
              assetName={assetName}
              projectLogo={projectLogo}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <Table />
          </Col>
        </Row>
      </ErrorBoundry>
    </Container>
  );
}

Auction.propTypes = {
  assetName: PropTypes.string.isRequired,
  projectLogo: PropTypes.element.isRequired,
  projectUrls: PropTypes.object,
};

export default memo(Auction);
