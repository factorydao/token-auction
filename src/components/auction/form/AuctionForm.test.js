import React from "react";
import renderer from "react-test-renderer";
import FVTTokenLogo from "components/logos/FVTTokenLogo";
import AuctionForm from "./AuctionForm";

test("should match snapshot", () => {
  const tree = renderer
    .create(<AuctionForm assetName="FVT" projectLogo={<FVTTokenLogo />} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
