import classnames from "classnames";
import Spinner from "components/indicators/spinner";
import Modal from "components/modals/success";
import { ethInstance, fromWei } from "evm-chain-scripts";
import { Form, Formik } from "formik";
import { useGlobalState } from "globalState";
import numeral from "numeral";
import PropTypes from "prop-types";
import {
  useBuyToken,
  useCurrentPrice,
  useLotNumber,
  useTokensLeftInLot,
  useTotalLots,
} from "queries/contract";
import { memo, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import { formatNumber, FormattedNumber } from "utils/formats";
import networks from "utils/networks.json";
import * as Yup from "yup";
import "./AuctionForm.scss";

const inputsValidation = Yup.object({
  maxSpend: Yup.number().required("Invalid max spend"),
  maxPrice: Yup.string().required("Invalid max price"),
  consent: Yup.bool().oneOf(
    [true],
    "Please agree to the terms before purchase"
  ),
});

function formatLotNumber(lotNumber) {
  return numeral(lotNumber).format("00");
}

const AuctionForm = ({ className, assetName, projectLogo }) => {
  const [address] = useGlobalState("address");
  const [currentNetwork] = useGlobalState("currentNetwork");
  const { symbol, explorer, availableToken } = networks[currentNetwork];
  const { data: lotNumber } = useLotNumber();
  const { data: totalLots, isLoading: isTotalLotsLoading } = useTotalLots();
  const { data: currentPrice } = useCurrentPrice();
  const { data: availableSupply } = useTokensLeftInLot();
  const buyToken = useBuyToken();
  const [error, setError] = useState(null);
  const [transactionHash, setTransactionHash] = useState(null);
  const [tokensAcquired, setTokensAcquired] = useState(null);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [maxPriceChanged, setMaxPriceChanged] = useState(false);

  async function onSubmit(values, { resetForm }) {
    if (!values.consent)
      return console.warn("Haulting form submission. Consent was not given");

    try {
      const { tx, tokensAcquired } = await buyToken(
        String(values.maxSpend),
        values.maxPrice
      );

      setTransactionHash(tx);
      const defaultValues = {
        maxSpend: 0,
        maxPrice: currentPrice ?? 0,
        consent: false,
      };

      setTokensAcquired(tokensAcquired);
      setShowSuccessModal(true);
      setError(null);
      resetForm({ values: defaultValues });
    } catch (err) {
      const readWeb3 = ethInstance.readWeb3(currentNetwork);
      // eslint-disable-next-line no-unused-vars
      const balance = fromWei((await readWeb3.getBalance(address)).toString());
      if (Number(values.maxSpend) > Number(balance)) {
        return setError(`You don't have enough ${symbol}`);
      }
      setError(
        <div>
          {
            "Oops, looks like there was a problem with your transaction. Please try again or contact us on "
          }
          <a
            href="https://discord.com/invite/factorydao"
            target="_blank"
            rel="noreferrer"
            className="link"
          >
            discord
          </a>
        </div>
      );
      throw err;
    }
  }

  const handleSuccessModalHide = () => setShowSuccessModal(false);

  const roundedCurrentPrice = currentPrice
    ? parseFloat(currentPrice).toFixed(6)
    : 0.0;

  const Error = ({ error, touched }) =>
    touched &&
    error && (
      <div style={{ color: "red", fontSize: 18, textTransform: "none" }}>
        {error}
      </div>
    );

  const LotNumber = () =>
    lotNumber === -1 ? (
      <>SOLD OUT</>
    ) : (
      <>
        LOT {formatLotNumber(lotNumber)}&nbsp;
        <span className="muted">/ {formatLotNumber(totalLots)}</span>
      </>
    );

  return (
    <>
      {!error ? (
        <Modal show={showSuccessModal} onHide={handleSuccessModalHide}>
          {transactionHash ? (
            <>
              <p>
                <strong>
                  You have bought {formatNumber(tokensAcquired)}{" "}
                  {availableToken || "FVT"} tokens
                </strong>
              </p>
              <p>
                You can track your transaction
                <br />
                <a
                  className="word-break-all"
                  href={`${explorer}/tx/${transactionHash}`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {transactionHash}
                </a>
              </p>
            </>
          ) : null}
        </Modal>
      ) : null}
      <Formik
        initialValues={{
          maxSpend: 0,
          maxPrice: 0,
          consent: false,
        }}
        validationSchema={inputsValidation}
        onSubmit={onSubmit}
        validateOnBlur
        validateOnChange
      >
        {({
          values,
          errors,
          touched,
          isSubmitting,
          getFieldProps,
          setFieldValue,
        }) => {
          useEffect(() => {
            if (currentPrice && !maxPriceChanged)
              setFieldValue("maxPrice", currentPrice);
          }, [currentPrice]);

          return (
            <Form className={classnames("auction-form", className)}>
              {isSubmitting ? <Spinner overlay={true} /> : null}
              <Row className="auction-form__header">
                <Col>
                  <span className="auction-form__header__project-logo">
                    {projectLogo}
                  </span>
                  &nbsp;
                  <span className="auction-form__header__asset-name">
                    {assetName}
                  </span>
                </Col>
                <Col className="auction-form__lot text-right">
                  {isTotalLotsLoading ? <Spinner size="sm" /> : <LotNumber />}
                </Col>
              </Row>
              <Row className="d-flex justify-content-sm-between">
                <Col xs="auto">
                  <label className="auction-form__label">
                    Current price
                    <div className="auction-form__value">
                      {currentPrice ? (
                        <FormattedNumber
                          number={roundedCurrentPrice}
                          suffix={symbol}
                        />
                      ) : null}
                    </div>
                  </label>
                </Col>
                <Col xs="auto" className="d-flex">
                  <label className="auction-form__label">
                    Remaining lot supply
                    <div className="auction-form__value">
                      {availableSupply ? (
                        <FormattedNumber
                          number={availableSupply}
                          format={"0,0.[0000]"}
                        />
                      ) : null}
                    </div>
                  </label>
                </Col>
              </Row>
              {address ? (
                <>
                  <Row>
                    <Col>
                      <InputGroup>
                        <label className="auction-form__label">
                          Max spend
                          <input
                            type="number"
                            name="maxSpend"
                            {...getFieldProps("maxSpend")}
                            className="form-control"
                            style={{
                              width: "calc(100% - 57px)",
                              display: "inline-block",
                            }}
                            onWheel={(e) => e.target.blur()}
                          />
                          <InputGroup
                            style={{
                              display: "inline-block",
                              fontSize: "21px",
                              width: "57px",
                              borderBottom: "none",
                              paddingBottom: 0,
                            }}
                          >
                            {symbol}
                          </InputGroup>
                          <Error
                            error={errors?.maxSpend}
                            touched={touched.maxSpend}
                          />
                        </label>
                      </InputGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="auction-form__max-price-control">
                      <InputGroup>
                        <div className="auction-form__label">Max price</div>
                        <input
                          className="max-price-value form-control"
                          type="number"
                          name="maxPrice"
                          {...getFieldProps("maxPrice")}
                          onWheel={(e) => e.target.blur()}
                          onChange={(e) => {
                            setFieldValue("maxPrice", e.target.value);
                            setMaxPriceChanged(true);
                          }}
                        />
                        <div className="auction-form__symbol">{symbol}</div>
                        <Error
                          error={errors?.maxPrice}
                          touched={touched.maxPrice}
                        />
                      </InputGroup>
                    </Col>
                  </Row>
                  {/* <Row>
              <Col xs="auto">
                <label className="auction-form__label">
                  Expected Order Size
                  <div className="auction-form__value">
                    {currentPrice && (
                      <FormattedNumber
                        number={maxSpend / currentPrice}
                        suffix={assetName}
                      />
                    )}
                  </div>
                </label>
              </Col>
            </Row> */}
                  <Row>
                    <Col className="terms">
                      <input
                        type="checkbox"
                        name="consent"
                        checked={values.consent}
                        className="form-check"
                        {...getFieldProps("consent")}
                      ></input>
                      <div className="terms-description">
                        I understand &amp; have read all the white paper,&nbsp;
                        disclaimers etc. I know what I&apos;m doing,&nbsp;
                        <span className="highlight">let me buy</span>
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="text-right">
                      <Button
                        type="submit"
                        disabled={isSubmitting || Number(availableSupply) === 0}
                      >
                        Buy {assetName}
                      </Button>
                    </Col>
                  </Row>
                  <Row className="auction-form__feedback">
                    <Error
                      error={
                        (error && (error.message || error)) || errors?.consent
                      }
                      touched={touched.consent}
                    />
                  </Row>
                </>
              ) : null}
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

AuctionForm.propTypes = {
  className: PropTypes.string,
  assetName: PropTypes.string.isRequired,
  projectLogo: PropTypes.element.isRequired,
};

export default memo(AuctionForm);
