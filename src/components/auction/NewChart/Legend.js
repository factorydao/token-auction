import classnames from "classnames";
import { BLOCK_TYPES } from "components/d3Chart/helpers";
import Arrow from "../../graph/grid/arrow.svg";
import Text from "../../graph/grid/text.svg";
import styles from "./Legend.module.scss";

const GridLegendItem = ({ type, label }) => {
  return (
    <div className={styles.legendItem}>
      <div className={classnames(styles.point, styles[type])}>
        {type === BLOCK_TYPES.CLOSED && "X"}
      </div>
      <span className={styles.text}>{label}</span>
    </div>
  );
};
const ScrollArrow = () => {
  return (
    <div className={styles.arrow}>
      <img src={Arrow} alt="arrow_right" />
      <img src={Text} alt="text_arrow" />
    </div>
  );
};

const Legend = () => {
  return (
    <div className={styles.legend}>
      <GridLegendItem type={BLOCK_TYPES.OPEN} label="Sold" />
      <GridLegendItem type={BLOCK_TYPES.CLOSED} label="Lot sold out" />
      <GridLegendItem type={BLOCK_TYPES.ACTIVE} label="Buy" />
      <ScrollArrow />
    </div>
  );
};

export default Legend;
