import {
  BarController,
  BarElement,
  CategoryScale,
  Chart,
  LinearScale,
  LineElement,
  LogarithmicScale,
  PointElement,
  ScatterController,
  Tooltip,
} from "chart.js";
import zoomPlugin from "chartjs-plugin-zoom/dist/chartjs-plugin-zoom.js";
import { memo, useEffect, useRef } from "react";
import { antiFlicker } from "./helpers/plugin";
import { externalTooltipHandler } from "./Tooltip";
Chart.register(
  zoomPlugin,
  antiFlicker,
  ScatterController,
  BarController,
  CategoryScale,
  BarElement,
  LogarithmicScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip
);

const NewChart = ({ data, marks }) => {
  const Ctx = useRef();
  const chart = useRef();

  const options = {
    responsive: true,
    scales: {
      y: {
        beginAtZero: true,
        ticks: {
          color: "#8f8f8f",
          font: {
            size: 14,
            family: "Work Sans",
          },
          stepSize: 1,
          callback: (tickValue) => `${marks[tickValue].label}`,
        },
        grid: {
          display: false,
          borderColor: "white",
        },
        type: "linear",
        offset: true,
      },
      y2: {
        offset: false,
        display: false,
      },
      x: {
        ticks: {
          display: false,
        },
        min: ({ chart }) => {
          const dataLength = chart.data.datasets?.[0].data.length;
          return dataLength > 30 ? dataLength - 30 : 0;
        },
        grid: {
          display: false,
          borderColor: "white",
        },
        offset: true,
      },
    },
    animation: {
      duration: 0,
    },
    plugins: {
      tooltip: {
        displayColors: false,
        filter: (dataset) => dataset.datasetIndex === 0,
        enabled: false,
        external: externalTooltipHandler,
      },
      zoom: {
        pan: {
          enabled: true,
          mode: "x",
          threshold: 10,
          onPanComplete: ({ chart }) => chart.update("none"),
        },
        zoom: {
          wheel: {
            enabled: true,
            speed: 0.05,
          },
          pinch: {
            enabled: true,
          },
          mode: "x",
          onZoomComplete: ({ chart }) => {
            const zoomLevel = chart?.getZoomLevel();
            if (zoomLevel < 0.6 || zoomLevel > 1.4)
              return chart.resetZoom(chart, "none");
            return chart.update("none");
          },
          onZoomStart({ chart, event }) {
            const zoomLevel = chart?.getZoomLevel();
            if (zoomLevel < 0.6) return event.wheelDelta > 0;
            if (zoomLevel > 1.4) return event.wheelDelta < 0;
          },
        },
        limits: {
          x: { min: 0, max: "original" },
        },
      },
    },
    layout: {
      padding: 10,
    },
    onHover: ({ chart }, [element]) => {
      if (element?.datasetIndex === 0) {
        chart?.setActiveElements([{ datasetIndex: 1, index: element?.index }]);
      } else {
        chart?.setActiveElements([]);
      }
    },
  };
  useEffect(() => {
    const ctx = Ctx.current.getContext("2d");
    chart.current = new Chart(ctx, {
      type: "scatter",
      data: data,
      options,
    });
    return () => chart.current.destroy();
  }, [data.datasets?.[0].data.length]);
  return (
    <div>
      <canvas
        ref={Ctx}
        style={{
          backgroundColor: "#14120f",
        }}
        height="100%"
        width="100%"
      />
    </div>
  );
};

export default memo(
  NewChart,
  (prev, next) => JSON.stringify(prev) === JSON.stringify(next)
);
