import moment from "moment";
import { formatNumber } from "utils/formats";

const generateInnerTooltipInfo = ({
  timestamp,
  trancheNumber,
  amount,
  price,
  symbol,
  cost,
}) => {
  return `
  <span style="color: var(--light-grey);">
  ${moment(timestamp).format("HH:mm:ss L")}
  </span>
  <br />
  Tranche: ${trancheNumber}
  <br />
  Amount: ${formatNumber(amount)}
  <br />
  Price: ${formatNumber(price)} ${symbol || "ETH"}
  <br />
  Cost: ${formatNumber(cost)} ${symbol || "ETH"}
  `;
};

const getOrCreateTooltip = (chart) => {
  let tooltipEl = chart.canvas.parentNode.querySelector("div");
  if (!tooltipEl) {
    tooltipEl = document.createElement("div");
    tooltipEl.style.background = "rgba(0, 0, 0, 0.7)";
    tooltipEl.style.borderRadius = "3px";
    tooltipEl.style.color = "white";
    tooltipEl.style.opacity = 1;
    tooltipEl.style.pointerEvents = "none";
    tooltipEl.style.position = "absolute";
    tooltipEl.style.transform = "translate(-50%, 0)";
    tooltipEl.style.transition = "all .1s ease";
    tooltipEl.style.padding = "";
    tooltipEl.style.fontFamily = '"Work Sans",sans-serif';

    const toolTipContent = document.createElement("div");
    toolTipContent.style.margin = "0px";
    toolTipContent.className = "content";

    toolTipContent.style.backgroundColor = "var(--dark)";
    toolTipContent.style.border = "1px solid var(--pink)";
    toolTipContent.style.lineHeight = "1rem";
    toolTipContent.style.width = "fit-content";
    toolTipContent.style.padding = "0.6rem 0.6rem 1rem";
    const isDesktop = window.matchMedia("(min-width: 768px)").matches;
    toolTipContent.style.fontSize = isDesktop ? "0.8rem" : "0.6rem";

    tooltipEl.appendChild(toolTipContent);
    chart.canvas.parentNode.appendChild(tooltipEl);
  }
  return tooltipEl;
};

export const externalTooltipHandler = (context) => {
  const { chart, tooltip } = context;
  const tooltipEl = getOrCreateTooltip(chart);

  // Hide if no tooltip  or is last one
  if (tooltip.opacity === 0 || tooltip.dataPoints[0]?.raw?.isLastOne) {
    return (tooltipEl.style.opacity = 0);
  }

  if (tooltip.body) {
    if (!tooltip.dataPoints[0]) return;
    const tooltipData = tooltip.dataPoints[0]?.raw.tooltip;
    const contentDiv = tooltipEl.querySelector("div.content");
    // Remove old innerHTML
    while (contentDiv.innerHTML) {
      contentDiv.innerHTML = "";
    }
    contentDiv.innerHTML = generateInnerTooltipInfo(tooltipData);
  }

  const { offsetLeft: positionX, offsetTop: positionY } = chart.canvas;

  // Display, position
  tooltipEl.style.opacity = 1;
  tooltipEl.style.left = positionX + tooltip.caretX + "px";
  tooltipEl.style.top = positionY + tooltip.caretY + "px";
};
