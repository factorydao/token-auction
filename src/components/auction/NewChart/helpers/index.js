import { max } from "d3";

const SCSS_VARS = {
  pink: "#da0060",
  yellow: "#fdf446",
  lightBg: "#252525",
};

const generateScatterChartSet = (values) => {
  return {
    label: "scatterPlot",
    type: "scatter",
    data: values,
    pointBackgroundColor: (point) => {
      if (point.raw?.isLastOne) return SCSS_VARS.yellow;
      return SCSS_VARS.pink;
    },
    pointStyle: (point) => {
      if (point.raw?.isLastOne || point.raw?.tokensLeftInTranche) {
        return "rect";
      } else {
        return drawX(point);
      }
    },
    pointRadius: (point) => {
      const zoomRatio = point.chart.getZoomLevel();
      return 10 * zoomRatio;
    },
    hoverRadius: (point) => {
      const zoomRatio = point.chart.getZoomLevel();
      return 10 * zoomRatio;
    },
  };
};

const drawX = (point, color = SCSS_VARS.yellow) => {
  let shape = document.createElement("canvas");
  const scale = point?.chart.getZoomLevel();
  const sideLength = 13 * scale;
  const topCoord = 2 * scale;
  const bottomCoord = 11 * scale;
  shape.width = sideLength;
  shape.height = sideLength;
  const ctx = shape.getContext("2d");
  ctx.fillStyle = SCSS_VARS.pink;
  ctx.fillRect(0, 0, sideLength, sideLength);
  ctx.beginPath();
  ctx.moveTo(topCoord, topCoord);
  ctx.lineTo(bottomCoord, bottomCoord);
  ctx.stroke();
  ctx.moveTo(bottomCoord, topCoord);
  ctx.lineTo(topCoord, bottomCoord);
  ctx.strokeStyle = color;
  ctx.stroke();
  const canvasImg = shape.toDataURL("image/png", 0.92);
  const img = new Image(sideLength, sideLength);
  img.src = canvasImg;
  return img;
};

const generateVolumeChartInBackground = (data, maxY, volumeMax) => {
  return {
    label: "volumes",
    type: "bar",
    yAxisID: "y2",
    data: data.map((item) => {
      let y;
      if (item.volume) {
        const perc = item.volume / volumeMax;
        const res = maxY * perc;
        y = res;
      } else {
        y = 0;
      }
      return { y, x: item.x };
    }),
    backgroundColor: (point) => {
      if (point.active) {
        return SCSS_VARS.pink;
      }
      return SCSS_VARS.lightBg;
    },
    pointHitRadius: 0,
    barThickness: (bar) => {
      const zoomRatio = bar.chart.getZoomLevel();
      return 12 * zoomRatio;
    },
  };
};

export const generateDataSets = (data, maxY) => {
  if (!data) return;
  const volumeMax = max(data, (data) => data.volume);
  const datasets = [];
  datasets.push(generateScatterChartSet(data));
  datasets.push(generateVolumeChartInBackground(data, maxY, volumeMax));
  return datasets;
};

export const mapBlockY = (arr, numToFind) => {
  const _arr = arr.map((elem) => elem.value);
  const lowerNumber = _arr.reverse().find((elem) => elem <= numToFind);
  const { y } = arr.find((elem) => elem.value === lowerNumber);
  const result = y + numToFind / lowerNumber / 2;
  return isFinite(result) ? result : 0;
};
