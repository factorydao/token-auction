export const antiFlicker = {
  id: "anti-flicker",
  beforeDraw: (chart) => {
    const zoomlevel = chart.getZoomLevel();
    if (zoomlevel > 10 || zoomlevel === Infinity) return chart.zoom(chart);
  },
};
