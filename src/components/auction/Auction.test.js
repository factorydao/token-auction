import React from "react";
import renderer from "react-test-renderer";
import FVTTokenLogo from "components/logos/FVTTokenLogo";
import Auction from "./Auction";

test("should match snapshot", () => {
  const tree = renderer
    .create(
      <Auction
        assetName="FVT"
        totalSupply={20000000}
        projectLogo={<FVTTokenLogo />}
        projectUrls={{
          website: "https://finance.vote",
          medium: "https://medium.com/@financedotvote",
          telegram: "https://t.me/financedotvote",
          twitter: "https://twitter.com/financedotvote",
        }}
      />
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
