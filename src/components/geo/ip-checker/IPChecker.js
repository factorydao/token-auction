import React, { memo, useState, useEffect } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Spinner from "components/indicators/spinner";
import { useIPCheck } from "queries/geo";
import "./IPChecker.scss";

function IPChecker({ className, children }) {
  const [banned, setBanned] = useState(false),
    [countryCode, setCountryCode] = useState(false),
    { isLoading, error, data } = useIPCheck();

  useEffect(() => {
    if (data) {
      const { banned, countryCode } = data;

      setBanned(banned);
      setCountryCode(countryCode);
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  if (isLoading) {
    return <Spinner size="sm" />;
  }

  if (banned) {
    return (
      <div className={classnames("ip-checker", className)}>
        <p>Your country code: {countryCode}</p>
        <p>
          Apologies, your region has been geoblocked. You are not able to
          participate in our auction.
        </p>
      </div>
    );
  }

  return children;
}

IPChecker.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};

export default memo(IPChecker);
