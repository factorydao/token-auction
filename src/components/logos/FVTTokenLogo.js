import Image from "./FVTTokenLogo.svg";

function FVTTokenLogo() {
  return <img src={Image} alt="FVT" />;
}

export default FVTTokenLogo;
