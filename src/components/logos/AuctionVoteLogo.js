import PropTypes from "prop-types";
import Image from "./AuctionVoteLogo.svg";

function AuctionVoteLogo({ className }) {
  return (
    <img
      style={{ maxWidth: 160 }}
      className={className}
      src={Image}
      alt="auction.vote"
    />
  );
}

AuctionVoteLogo.propTypes = {
  className: PropTypes.string,
};

export default AuctionVoteLogo;
