import React, { memo, useState, useEffect, useCallback } from "react";
import PropTypes from "prop-types";

function ThreeDots({ className }) {
  const [numDots, setNumDots] = useState(1),
    [dots, setDots] = useState(null),
    iterate = useCallback(() => {
      let dots = "";

      for (let i = 0; i < numDots; i++) {
        dots += ".";
      }

      setDots(dots);
      setNumDots(numDots < 3 ? numDots + 1 : 0);
    }, [numDots]);

  useEffect(() => {
    const interval = setInterval(iterate, 200);

    return () => {
      clearInterval(interval);
    };
  }, [iterate]);

  return <span className={className}>{dots}</span>;
}

ThreeDots.propTypes = {
  className: PropTypes.string,
};

export default memo(ThreeDots);
