import React from "react";
import renderer from "react-test-renderer";
import ThreeDots from "./ThreeDots";

test("should match snapshot", () => {
  const tree = renderer.create(<ThreeDots />).toJSON();

  expect(tree).toMatchSnapshot();
});
