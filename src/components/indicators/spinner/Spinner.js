import React, { memo } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Image from "./spinner.gif";
import "./Spinner.scss";

function Spinner(props) {
  const spinnerProps = { ...props },
    { overlay } = props;

  delete spinnerProps.overlay;

  return (
    <div className={classnames("spinner", { "spinner--overlay": overlay })}>
      <img src={Image} alt="Loading..." />
    </div>
  );
}

Spinner.propTypes = {
  overlay: PropTypes.bool,
};

export default memo(Spinner);
