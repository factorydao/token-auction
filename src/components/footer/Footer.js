import discordIcon from "images/discord.svg";
import twitterIcon from "images/twitter.svg";
import styles from "./Footer.module.scss";

const Footer = () => {
  return (
    <footer className={styles["app-footer"]}>
      <div className={styles["app-footer__container"]}>
        <div>
          {"2022 All rights reserved  "}
          <span className={styles.factoryDao}>factoryDAO</span>
          <span className={styles["app-footer__version"]}>
            v. {process.env.REACT_APP_VERSION}
          </span>
        </div>
        <div className={styles["app-footer__links"]}>
          <a
            href="https://twitter.com/FactDAO"
            target="_blank"
            rel="noreferrer"
          >
            <img src={twitterIcon} alt="Twitter" />
          </a>
          <a
            href="https://discord.com/invite/factorydao"
            target="_blank"
            rel="noreferrer"
          >
            <img src={discordIcon} alt="Discord" />
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
