import React from "react";
import Image from "./Help.svg";

function Help() {
  return <img src={Image} alt="Help" />;
}

export default Help;
