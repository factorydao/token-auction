import React from "react";
import renderer from "react-test-renderer";
import Curve from "./Curve";

test("should match snapshot", () => {
  const tree = renderer.create(<Curve />).toJSON();

  expect(tree).toMatchSnapshot();
});
