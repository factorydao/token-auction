import Spinner from "components/indicators/spinner";
import PropTypes from "prop-types";
import { memo, useEffect, useState } from "react";
import { getEventsV2 } from "utils/curve";

function getCurrentTime() {
  return new Date().getTime();
}

function Curve({ children }) {
  const [error, setError] = useState(null),
    [waitingForEvents, setWaitingForEvents] = useState(false),
    [waitingForLongTime, setWaitingForLongTime] = useState(false),
    [, setWaitTime] = useState(0),
    [events, setEvents] = useState([]);

  useEffect(() => {
    let loadingIntervalCheck;

    function startLoading() {
      setWaitingForEvents(true);
      setWaitTime(getCurrentTime());

      loadingIntervalCheck = setInterval(() => {
        setWaitTime((waitTime) => {
          setWaitingForLongTime(getCurrentTime() - waitTime > 5000);

          return waitTime;
        });
      }, 1000);
    }

    function stopLoading() {
      setWaitingForEvents(false);
      setWaitTime(0);
      clearInterval(loadingIntervalCheck);
    }

    try {
      startLoading();

      // getEvents((event) =>
      //   setEvents((events) => {
      //     stopLoading();

      //     return [event, ...events];
      //   })
      // );
      getEventsV2().then((events) => {
        setEvents(events);
        stopLoading();
      });
    } catch (err) {
      console.error(err);
      setError(err);
    }

    return () => {
      stopLoading();
    };
  }, []);

  if (error) {
    return <pre>{error.stack || error.toString()}</pre>;
  }

  if (waitingForEvents) {
    return (
      <div>
        <Spinner size="sm" />
        &nbsp; Waiting for events...
        {waitingForLongTime && (
          <>&nbsp;this is taking longer than expected...</>
        )}
      </div>
    );
  }

  if (children) {
    return children(events);
  }

  return <pre>{JSON.stringify(events)}</pre>;
}

Curve.propTypes = {
  children: PropTypes.func,
};

export default memo(Curve);
