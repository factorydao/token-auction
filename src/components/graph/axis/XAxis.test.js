import React from "react";
import renderer from "react-test-renderer";
import XAxis from "./XAxis";

test("should match snapshot", () => {
  const tree = renderer.create(<XAxis />).toJSON();

  expect(tree).toMatchSnapshot();
});
