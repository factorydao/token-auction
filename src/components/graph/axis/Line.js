import SVGLine from "components/svg/line";
import PropTypes from "prop-types";
import { forwardRef, memo } from "react";
import { resolveUnits } from "utils/units";

export const ORIENTATIONS = {
  HORIZONTAL: "horizontal",
  VERTICAL: "vertical",
};

const Line = forwardRef(({ x, y, length, orientation }, ref) => {
  let x2 = x,
    y2 = y;

  switch (orientation) {
    default:
    case ORIENTATIONS.HORIZONTAL:
      x2 = resolveUnits(x, length);
      break;
    case ORIENTATIONS.VERTICAL:
      y2 = resolveUnits(y, length);
      break;
  }

  return <SVGLine ref={ref} x1={x} y1={y} x2={x2} y2={y2} />;
});
Line.displayName = "Line";
Line.propTypes = {
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  length: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  orientation: PropTypes.oneOf(Object.values(ORIENTATIONS)),
};

Line.defaultProps = {
  x: 0,
  y: 0,
  length: 100,
  orientation: ORIENTATIONS.HORIZONTAL,
};

export default memo(Line);
