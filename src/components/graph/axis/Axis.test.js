import React from "react";
import renderer from "react-test-renderer";
import Axis from "./Axis";

test("should match snapshot", () => {
  const tree = renderer.create(<Axis />).toJSON();

  expect(tree).toMatchSnapshot();
});
