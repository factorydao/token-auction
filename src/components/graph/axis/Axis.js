import Text from "components/svg/text";
import PropTypes from "prop-types";
import {
  forwardRef,
  Fragment,
  memo,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { formatNumber } from "utils/formats";
import { resolveUnits } from "utils/units";
import Line, { ORIENTATIONS } from "./Line";

const labelFormat = "0,0.000000";

const Axis = forwardRef(
  (
    {
      x,
      y,
      length,
      orientation,
      min,
      max,
      markSpacing,
      markSuffix,
      showLabels,
      showLine,
      showMarks,
      initialValue,
    },
    ref
  ) => {
    const lineRef = useRef(null),
      minLabelRef = useRef(null),
      maxLabelRef = useRef(null),
      marksRef = useRef(null),
      [lineX, setLineX] = useState(x),
      [lineY, setLineY] = useState(y),
      [minLabelX, setMinLabelX] = useState(0),
      [minLabelY, setMinLabelY] = useState(0),
      [maxLabelX, setMaxLabelX] = useState(0),
      [maxLabelY, setMaxLabelY] = useState(0),
      isHorizontal = useMemo(
        () => orientation === ORIENTATIONS.HORIZONTAL,
        [orientation]
      ),
      // , labelRange = useMemo(() => maxLabel - minLabel, [ minLabel, maxLabel ])
      markCount = useMemo(
        () => Math.ceil(length / markSpacing),
        [length, markSpacing]
      ),
      marks = useMemo(() => {
        const marks = [];

        if (showMarks) {
          for (let i = showLabels ? 1 : 0; i < markCount - 1; i++) {
            const position = markSpacing * i + markSpacing * 0.4,
              // , value = (position / length) * labelRange
              value = 2 ** i * initialValue;

            marks.push({
              x: isHorizontal ? position : 0,
              y: isHorizontal ? 0 : length - position,
              label:
                formatNumber(value, labelFormat) +
                (markSuffix ? ` ${markSuffix}` : ""),
            });
          }
        }

        return marks;
      }, [
        showMarks,
        showLabels,
        length,
        markCount,
        markSpacing,
        markSuffix,
        isHorizontal,
        initialValue,
      ]),
      spacing = 2;

    useEffect(() => {
      const lineBox = lineRef.current ? lineRef.current.getBBox() : {},
        maxLabelBox = maxLabelRef.current ? maxLabelRef.current.getBBox() : {};

      let lineX, lineY, minLabelX, minLabelY, maxLabelX, maxLabelY;

      switch (orientation) {
        default:
        case ORIENTATIONS.HORIZONTAL:
          lineX = resolveUnits(x);
          lineY = resolveUnits(y);
          minLabelX = resolveUnits(lineX);
          minLabelY = resolveUnits(lineY, lineBox.height, spacing);
          maxLabelX = resolveUnits(lineX, length);
          maxLabelY = resolveUnits(lineY, lineBox.height, spacing);
          break;
        case ORIENTATIONS.VERTICAL:
          maxLabelX = resolveUnits(x);
          maxLabelY = resolveUnits(y);
          lineX = resolveUnits(maxLabelX, maxLabelBox.width, spacing);
          lineY = resolveUnits(maxLabelY);
          minLabelX = resolveUnits(maxLabelX, maxLabelBox.width);
          minLabelY = resolveUnits(maxLabelY, length, spacing);
          break;
      }

      setLineX(lineX);
      setLineY(lineY);
      setMinLabelX(minLabelX);
      setMinLabelY(minLabelY);
      setMaxLabelX(maxLabelX);
      setMaxLabelY(maxLabelY);
    }, [lineRef, maxLabelRef, orientation, x, y, length]);

    return (
      <g ref={ref}>
        {showLine && (
          <Line
            ref={lineRef}
            x={lineX}
            y={lineY}
            length={length}
            orientation={orientation}
          />
        )}
        <g ref={marksRef}>
          {marks.map(({ x, y, label }, index) => (
            <Text
              key={`mark${index}`}
              x={resolveUnits(x, lineX)}
              y={resolveUnits(y, lineY)}
              textAnchor="end"
              alignmentBaseline="baseline"
            >
              {label}
            </Text>
          ))}
        </g>
        {showLabels && (
          <Fragment>
            <Text
              ref={minLabelRef}
              x={minLabelX}
              y={minLabelY}
              dominantBaseline={isHorizontal ? "hanging" : "auto"}
              textAnchor={isHorizontal ? "start" : "end"}
            >
              {formatNumber(min, labelFormat)}
            </Text>
            <Text
              ref={maxLabelRef}
              x={maxLabelX}
              y={maxLabelY}
              dominantBaseline="hanging"
              textAnchor={isHorizontal ? "end" : "start"}
            >
              {formatNumber(max, labelFormat)}
            </Text>
          </Fragment>
        )}
      </g>
    );
  }
);
Axis.displayName = "Axis";
Axis.propTypes = {
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  length: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  orientation: PropTypes.oneOf(Object.values(ORIENTATIONS)),
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  markSpacing: PropTypes.number,
  markLength: PropTypes.number,
  markSuffix: PropTypes.string,
  showLabels: PropTypes.bool,
  showLine: PropTypes.bool,
  showMarks: PropTypes.bool,
  initialValue: PropTypes.number,
};

Axis.defaultProps = {
  x: 0,
  y: 0,
  length: 100,
  orientation: ORIENTATIONS.HORIZONTAL,
  min: 0,
  max: 100,
  markSpacing: 10,
  markLength: 10,
  showLabels: true,
  showLine: true,
  showMarks: false,
};

export { ORIENTATIONS };

export default memo(Axis);
