import { forwardRef, memo } from "react";
import Axis, { ORIENTATIONS } from "./Axis";

const XAxis = forwardRef((props, ref) => {
  return <Axis {...props} ref={ref} orientation={ORIENTATIONS.HORIZONTAL} />;
});
XAxis.displayName = "XAxis";
export default memo(XAxis);
