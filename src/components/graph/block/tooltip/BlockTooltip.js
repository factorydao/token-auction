import classnames from "classnames";
import moment from "moment";
import PropTypes from "prop-types";
import { forwardRef, memo } from "react";
import { formatNumber } from "utils/formats";
import "./BlockTooltip.scss";

const BlockTooltip = forwardRef(
  (
    {
      className,
      x,
      y,
      height,
      width,
      timestamp,
      amount,
      price,
      cost,
      trancheNumber,
    },
    ref
  ) => {
    return (
      <foreignObject
        className={classnames("block-tooltip", className)}
        x={x}
        y={y}
        width={width}
        height={height}
      >
        <div className="block-tooltip__content" ref={ref}>
          <span className="block-tooltip__content--timestamp">
            {moment(timestamp).format("HH:mm:ss L")}
          </span>
          <br />
          Tranche: {trancheNumber}
          <br />
          Amount: {formatNumber(amount)}
          <br />
          Price: {formatNumber(price)} ETH
          <br />
          Cost: {formatNumber(cost)} ETH
        </div>
      </foreignObject>
    );
  }
);
BlockTooltip.displayName = "BlockTooltip";
BlockTooltip.propTypes = {
  className: PropTypes.string,
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  timestamp: PropTypes.number.isRequired,
  amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  cost: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

BlockTooltip.defaultProps = {
  x: 0,
  y: 0,
  height: "100%",
  width: "100%",
};

export default memo(BlockTooltip);
