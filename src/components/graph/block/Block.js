import React, { memo, useRef, useEffect, useState, useCallback } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Rect from "components/svg/rect";
import Text from "components/svg/text";
import {
  keepWithinBounds,
  keepWithinSVGBounds,
  findParentElement,
} from "utils/bounds";
import Tooltip from "./tooltip";
import "./Block.scss";

const TOOLTIP_PADDING = 4;

export const TYPES = {
  OPEN: "open",
  EMPTY: "empty",
  CLOSED: "closed",
  ACTIVE: "active",
};

function Block({
  type,
  x,
  y,
  height,
  width,
  tooltip,
  tooltipBounds,
  onMouseOver,
}) {
  const rectRef = useRef(null),
    tooltipRef = useRef(null),
    [tooltipX, setTooltipX] = useState(0),
    [tooltipY, setTooltipY] = useState(0),
    keepTootipWithinBounds = useCallback(
      (x, y) => {
        if (tooltip) {
          const tooltip = tooltipRef.current,
            bounds = tooltipBounds
              ? keepWithinBounds(tooltipBounds, {
                  x,
                  y,
                  height: tooltip.offsetHeight,
                  width: tooltip.offsetWidth,
                })
              : keepWithinSVGBounds(
                  { x, y },
                  tooltip,
                  findParentElement(tooltip, "svg")
                );

          return bounds;
        }

        return { x, y };
      },
      [tooltip, tooltipRef, tooltipBounds]
    ),
    isEmpty = type === TYPES.EMPTY,
    isClosed = type === TYPES.CLOSED,
    isActive = type === TYPES.ACTIVE;

  useEffect(() => {
    const rectBounds = rectRef.current
        ? rectRef.current.getBoundingClientRect()
        : {},
      tooltipX = -TOOLTIP_PADDING,
      tooltipY =
        (tooltipRef.current ? -tooltipRef.current.offsetHeight : 0) +
        rectBounds.height +
        TOOLTIP_PADDING,
      boundedTooltipBounds = keepTootipWithinBounds(tooltipX, tooltipY);

    setTooltipX(boundedTooltipBounds.x);
    setTooltipY(boundedTooltipBounds.y);
  }, [rectRef, tooltipRef, keepTootipWithinBounds, width, height, x]);
  return (
    <g
      className={classnames("block", {
        "block--empty": isEmpty,
        "block--closed": isClosed,
        "block--active": isActive,
      })}
      transform={`translate(${x},${y})`}
      onMouseOver={onMouseOver}
    >
      {tooltip && (
        <Tooltip
          ref={tooltipRef}
          className="block__tooltip"
          x={tooltipX}
          y={tooltipY}
          timestamp={tooltip.timestamp}
          amount={tooltip.amount}
          price={tooltip.price}
          cost={tooltip.cost}
          trancheNumber={tooltip.trancheNumber}
        />
      )}
      <Rect ref={rectRef} height={height} width={width} />
      {isClosed && (
        <Text
          x={width / 2}
          y={height / 1.7}
          dominantBaseline="middle"
          textAnchor="middle"
        >
          X
        </Text>
      )}
    </g>
  );
}

Block.propTypes = {
  type: PropTypes.oneOf(Object.values(TYPES)),
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  tooltip: PropTypes.shape({
    timestamp: PropTypes.number.isRequired,
    amount: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      .isRequired,
    price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    cost: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  }),
  tooltipBounds: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  }),
  onMouseOver: PropTypes.func,
};

Block.defaultProps = {
  type: TYPES.OPEN,
  x: 0,
  y: 0,
  height: 10,
  width: 10,
};

export default memo(Block);
