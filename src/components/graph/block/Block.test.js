import React from "react";
import renderer from "react-test-renderer";
import Block from "./Block";

test("should match snapshot", () => {
  const tree = renderer.create(<Block />).toJSON();

  expect(tree).toMatchSnapshot();
});
