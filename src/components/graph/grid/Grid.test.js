import React from "react";
import renderer from "react-test-renderer";
import { TYPES } from "components/graph/block";
import Grid from "./Grid";

test("should match snapshot", () => {
  const tree = renderer
    .create(
      <Grid
        minX={0}
        maxX={100}
        minY={0}
        maxY={100}
        width={300}
        height={300}
        matrix={[
          { x: 10, y: 10 },
          { x: 20, y: 10 },
          { x: 30, y: 10, type: TYPES.CLOSED },
          { x: 40, y: 30 },
          { x: 50, y: 30, type: TYPES.CLOSED },
          { x: 60, y: 50 },
          { x: 70, y: 50 },
          { x: 80, y: 50 },
          { x: 90, y: 50 },
          { x: 100, y: 50 },
          { x: 110, y: 50 },
          { x: 120, y: 50, type: TYPES.CLOSED },
          { x: 130, y: 70 },
          { x: 140, y: 70 },
          { x: 150, y: 70 },
          { x: 160, y: 70 },
          { x: 170, y: 70 },
          { x: 180, y: 70 },
          { x: 180, y: 60 },
          { x: 180, y: 50, type: TYPES.CLOSED },
          { x: 190, y: 190 },
          { x: 200, y: 190 },
          { x: 210, y: 190 },
          { x: 210, y: 150 },
          { x: 210, y: 130, type: TYPES.CLOSED },
          { x: 220, y: 250 },
          { x: 220, y: 240, type: TYPES.EMPTY },
        ]}
      />
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
