import classnames from "classnames";
import { TYPES } from "components/graph/block";
import PropTypes from "prop-types";
import { forwardRef, memo } from "react";
import "./GridLegend.scss";
import Item from "./GridLegendItem";

const GridLegend = forwardRef(({ className, x, y }, ref) => {
  return (
    <g
      ref={ref}
      className={classnames("grid-legend", className)}
      style={{ transform: `translate(${x}, ${y})` }}
    >
      <Item y="0rem" type={TYPES.OPEN} label="Sold" />
      <Item y="1.5rem" type={TYPES.CLOSED} label="Lot sold out" />
      <Item y="3rem" type={TYPES.ACTIVE} label="Buy" />
    </g>
  );
});
GridLegend.displayName = "GridLegend";
GridLegend.propTypes = {
  className: PropTypes.string,
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

GridLegend.defaultProps = {
  x: 0,
  y: 0,
};

export default memo(GridLegend);
