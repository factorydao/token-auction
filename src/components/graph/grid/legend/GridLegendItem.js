import React, { memo } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Block, { TYPES } from "components/graph/block";
import Text from "components/svg/text";
import "./GridLegendItem.scss";

function GridLegendItem({ className, x, y, type, label }) {
  return (
    <g
      className={classnames("grid-legend-item", className)}
      style={{ transform: `translate(${x}, ${y})` }}
    >
      <Block type={type} />
      <Text x={17} y="0.6rem">
        {label}
      </Text>
    </g>
  );
}

GridLegendItem.propTypes = {
  className: PropTypes.string,
  x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  type: PropTypes.oneOf(Object.values(TYPES)),
  label: PropTypes.string.isRequired,
};

GridLegendItem.defaultProps = {
  x: 0,
  y: 0,
};

export default memo(GridLegendItem);
