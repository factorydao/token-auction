import React from "react";
import renderer from "react-test-renderer";
import GridLegend from "./GridLegend";

test("should match snapshot", () => {
  const tree = renderer.create(<GridLegend />).toJSON();

  expect(tree).toMatchSnapshot();
});
