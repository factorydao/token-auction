import { useEffect, useState } from "react";
import useResizeAware from "react-resize-aware";
import { toBounds } from "utils/bounds";
import { resolveCoordinate } from "utils/units";

const useResizer = ({
  dataGridRef,
  legendRef,
  xAxisRef,
  yAxisRef,
  currentDataX,
  currentDataY,
  width,
  height,
  rangeX,
  rangeY,
  rangeYLabel,
  blockSize,
  minX,
  maxX,
  minY,
  maxY,
}) => {
  const [resizeListener, sizes] = useResizeAware();
  height =
    /%/.test(height) && typeof height === "string" ? sizes.height ?? 0 : height;
  width =
    /%/.test(width) && typeof width === "string" ? sizes.width ?? 0 : width;
  const [dataBounds, setDataBounds] = useState({
      x: 0,
      y: 0,
      height,
      width,
    }),
    [resolvedWidth, setResolvedWidth] = useState(
      typeof width === "string" ? sizes.width ?? 0 : width
    ),
    [resolvedHeight, setResolvedHeight] = useState(
      typeof height === "string" ? sizes.height ?? 0 : height
    ),
    [legendX, setLegendX] = useState(0),
    [legendY, setLegendY] = useState(0),
    [currentMinX, setCurrentMinX] = useState(minX),
    [currentMaxX, setCurrentMaxX] = useState(maxX),
    [currentMinY, setCurrentMinY] = useState(minY),
    [currentMaxY, setCurrentMaxY] = useState(maxY),
    [currentMinYLabel, setCurrentMinYLabel] = useState(minY),
    [currentMaxYLabel, setCurrentMaxYLabel] = useState(maxY),
    [xAxisX, setXAxisX] = useState(0),
    [xAxisY, setXAxisY] = useState(0),
    [yAxisX, setYAxisX] = useState(0),
    [yAxisY, setYAxisY] = useState(0),
    [ratioMinX, setRatioMinX] = useState(0),
    [ratioMaxX, setRatioMaxX] = useState(0),
    [ratioMinY, setRatioMinY] = useState(0),
    [ratioMaxY, setRatioMaxY] = useState(0);

  useEffect(() => {
    const xAxisBounds = xAxisRef.current ? xAxisRef.current.getBBox() : {},
      yAxisBounds = toBounds(
        yAxisRef.current ? yAxisRef.current.getBBox() : {}
      ),
      yAxisX = yAxisBounds.width,
      yAxisY = 0,
      xAxisX = yAxisX + blockSize,
      xAxisY = resolvedHeight - xAxisBounds.height - 1;

    setXAxisX(xAxisX);
    setXAxisY(xAxisY);
    setYAxisX(yAxisX);
    setYAxisY(yAxisY);
  }, [xAxisRef, yAxisRef, resolvedHeight, blockSize]);

  useEffect(() => {
    const dataBoundsLeft = xAxisX,
      dataBoundsWidth = resolvedWidth - dataBoundsLeft,
      dataBoundsHeight = xAxisY - 1,
      dataBounds = toBounds({
        x: dataBoundsLeft,
        y: yAxisY,
        width: resolveCoordinate(dataBoundsWidth),
        height: resolveCoordinate(dataBoundsHeight),
      });

    setDataBounds(dataBounds);
  }, [xAxisX, xAxisY, yAxisY, resolvedWidth]);

  useEffect(() => {
    const dataGridBounds = dataGridRef.current
        ? dataGridRef.current.getBBox()
        : {},
      dataGridBoundsWidth = resolveCoordinate(dataGridBounds.width),
      dataGridBoundsHeight = resolveCoordinate(dataGridBounds.height),
      ratioMinX = Math.abs(currentDataX / dataGridBoundsWidth),
      ratioMaxX = Math.abs(
        (currentDataX + dataBounds.width) / dataGridBoundsWidth
      ),
      ratioMinY = Math.abs(currentDataY / dataGridBoundsHeight),
      ratioMaxY = Math.abs(
        (currentDataY - dataBounds.height) / dataGridBoundsHeight
      );

    setRatioMinX(ratioMinX);
    setRatioMaxX(ratioMaxX);
    setRatioMinY(ratioMinY);
    setRatioMaxY(ratioMaxY);
  }, [dataGridRef, currentDataX, currentDataY, dataBounds]);
  useEffect(() => {
    setResolvedWidth(width);
    setResolvedHeight(height);
  }, [sizes, sizes.width, sizes.height, width, height]);

  useEffect(() => {
    const legendBounds = legendRef.current ? legendRef.current.getBBox() : {},
      legendX = resolveCoordinate(resolvedWidth) - legendBounds.width,
      legendY = 0;

    setLegendX(legendX);
    setLegendY(legendY);
  }, [legendRef, resolvedWidth]);

  useEffect(() => {
    const currentMinX = rangeX * ratioMinX,
      currentMaxY = rangeY * ratioMaxY,
      currentMaxX = rangeX * ratioMaxX,
      currentMinY = rangeY * ratioMinY;

    setCurrentMinX(currentMinX);
    setCurrentMaxX(currentMaxX);
    setCurrentMinY(currentMinY);
    setCurrentMaxY(currentMaxY);
  }, [rangeX, rangeY, ratioMinX, ratioMaxX, ratioMinY, ratioMaxY]);

  useEffect(() => {
    const currentMinYLabel = rangeYLabel * ratioMinY,
      currentMaxYLabel = rangeYLabel * ratioMaxY;

    setCurrentMinYLabel(currentMinYLabel);
    setCurrentMaxYLabel(currentMaxYLabel);
  }, [rangeYLabel, ratioMinY, ratioMaxY]);

  return {
    resizeListener,
    dataBounds,
    resolvedWidth,
    resolvedHeight,
    legendX,
    legendY,
    currentMinX,
    currentMaxX,
    currentMinY,
    currentMaxY,
    currentMinYLabel,
    currentMaxYLabel,
    xAxisX,
    xAxisY,
    yAxisX,
    yAxisY,
  };
};
export default useResizer;
