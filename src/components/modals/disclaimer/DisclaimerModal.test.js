import React from "react";
import renderer from "react-test-renderer";
import DisclaimerModal from "./DisclaimerModal";

test("should match snapshot", () => {
  const tree = renderer.create(<DisclaimerModal />).toJSON();

  expect(tree).toMatchSnapshot();
});
