import classnames from "classnames";
import Modal from "components/modals/modal";
import PropTypes from "prop-types";
import { memo } from "react";
import Icon from "./success.svg";
import "./SuccessModal.scss";

function SuccessModal(props) {
  const { className } = props,
    modalProps = { ...props };

  return (
    <Modal
      icon={Icon}
      title="Success!"
      {...modalProps}
      className={classnames("success-modal", className)}
    />
  );
}

SuccessModal.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};

export default memo(SuccessModal);
