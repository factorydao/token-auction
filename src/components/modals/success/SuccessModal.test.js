import React from "react";
import renderer from "react-test-renderer";
import SuccessModal from "./SuccessModal";

test("should match snapshot", () => {
  const tree = renderer.create(<SuccessModal />).toJSON();

  expect(tree).toMatchSnapshot();
});
