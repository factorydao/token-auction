import numeral from "numeral";
import PropTypes from "prop-types";

export const formatHash = (hash) =>
  hash && `${hash.substring(0, 5)}...${hash.substring(hash.length - 3)}`;

export const FormattedHash = ({ hash }) => (
  <span title={hash}>{formatHash(hash)}</span>
);

FormattedHash.propTypes = {
  hash: PropTypes.string.isRequired,
};

export const formatNumber = (number, format = "0,0.[000000000000000000]") => {
  const formattedNumber = number && numeral(number).format(format);

  return isNaN(parseFloat(formattedNumber)) ? number : formattedNumber;
};

export const FormattedNumber = ({ number, format, suffix }) => {
  const resolvedSuffix = typeof suffix === "undefined" ? "" : ` ${suffix}`,
    formattedNumber = formatNumber(number, format);

  return (
    <span title={`${formattedNumber}${resolvedSuffix}`}>
      {formattedNumber + resolvedSuffix}
    </span>
  );
};

FormattedNumber.propTypes = {
  number: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  format: PropTypes.string,
  suffix: PropTypes.string,
};
