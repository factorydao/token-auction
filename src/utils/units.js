function resolveUnit(unit) {
  if (typeof unit === "undefined" || unit === null) {
    return { value: 0 };
  }

  if (typeof unit === "string") {
    const parts = /^(\d+?(\.?\d+)?)?(\D*)?$/.exec(unit);

    if (parts) {
      const [_, value, __, suffix] = parts, // eslint-disable-line no-unused-vars
        parsedValue = parseFloat(value);

      return { value: isNaN(parsedValue) ? 0 : parsedValue, suffix };
    }

    return { value: 0 };
  }

  return { value: parseFloat(unit) };
}

export const resolveUnits = (...units) =>
  units.length === 0
    ? 0
    : units.reduce((unit, currentUnit) => {
        const resolvedCurrentUnit = resolveUnit(currentUnit);

        let resolvedUnit = unit;

        if (typeof resolvedUnit !== "object") {
          resolvedUnit = resolveUnit(unit);
        }

        return (
          resolvedUnit.value +
          resolvedCurrentUnit.value +
          (resolvedUnit.suffix || resolvedCurrentUnit.suffix || "")
        );
      }, resolveUnit(0));

export const resolveCoordinate = (value) => {
  if (typeof value !== "number") {
    return resolveCoordinate(parseFloat(value));
  }

  return isNaN(value) ? 0 : Math.max(value, 0);
};
