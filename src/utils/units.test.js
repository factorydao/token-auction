import { resolveUnits } from "./units";

describe("utils/units", () => {
  describe("resolveUnits", () => {
    it("should resolve a single number to a string", () => {
      expect(resolveUnits(1)).toEqual("1");
      expect(resolveUnits(10)).toEqual("10");
      expect(resolveUnits(10.0)).toEqual("10");
      expect(resolveUnits(10.01)).toEqual("10.01");
    });

    it("should add multiple numbers and resolve to a string", () => {
      expect(resolveUnits(1, 1)).toEqual("2");
      expect(resolveUnits(10, 1)).toEqual("11");
      expect(resolveUnits(10.0, 1)).toEqual("11");
      expect(resolveUnits(10.01, 1)).toEqual("11.01");
    });

    it("should resolve a single number with a suffix to a string", () => {
      expect(resolveUnits("1px")).toEqual("1px");
      expect(resolveUnits("10px")).toEqual("10px");
      expect(resolveUnits("10.00px")).toEqual("10px");
      expect(resolveUnits("10.01px")).toEqual("10.01px");
    });

    it("should add multiple numbers with a suffix and resolve to a string", () => {
      expect(resolveUnits("1px", "1px")).toEqual("2px");
      expect(resolveUnits("10px", "1px")).toEqual("11px");
      expect(resolveUnits("10.00px", "1px")).toEqual("11px");
      expect(resolveUnits("10.01px", "1px")).toEqual("11.01px");
    });

    it("should add multiple numbers with diffrent suffixes and resolve to a string with the first suffix", () => {
      expect(resolveUnits("1px", "1rem")).toEqual("2px");
      expect(resolveUnits("10px", "1rem")).toEqual("11px");
      expect(resolveUnits("10.00px", "1rem")).toEqual("11px");
      expect(resolveUnits("10.01px", "1rem")).toEqual("11.01px");
    });

    it("should add multiple numbers and strings with suffixes and resolve to a string with the first suffix", () => {
      expect(resolveUnits(1, "1px")).toEqual("2px");
      expect(resolveUnits(10, "1px")).toEqual("11px");
      expect(resolveUnits(10.0, "1px")).toEqual("11px");
      expect(resolveUnits(10.01, "1px")).toEqual("11.01px");

      expect(resolveUnits("1px", 1)).toEqual("2px");
      expect(resolveUnits("10px", 1)).toEqual("11px");
      expect(resolveUnits("10.00px", 1)).toEqual("11px");
      expect(resolveUnits("10.01px", 1)).toEqual("11.01px");
    });

    it("should return 0 if the input has no numerical value", () => {
      expect(resolveUnits()).toEqual(0);
    });

    it("should return 0 if the input in null or undefined", () => {
      expect(resolveUnits(null)).toEqual("0");
      expect(resolveUnits(null, null)).toEqual("0");
      expect(resolveUnits(undefined)).toEqual("0");
      expect(resolveUnits(undefined, undefined)).toEqual("0");
    });

    it("should return 0 with a suffix if the input has no numerical value but is a valid string", () => {
      expect(resolveUnits("px")).toEqual("0px");
      expect(resolveUnits("px", "px")).toEqual("0px");
      expect(resolveUnits("px", "1px")).toEqual("1px");
    });

    it("should return a string with a suffix if a non numerical value but is a valid string is provided as the first argument", () => {
      expect(resolveUnits("px", 1)).toEqual("1px");
    });
  });
});
