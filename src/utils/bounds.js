import assert from "assert";

export const toBounds = ({ x, y, width, height }) => {
  return {
    x,
    y,
    width,
    height,
    left: x,
    top: y,
    right: x + width,
    bottom: y + height,
  };
};

export const findParentElement = (element, parentTagName) => {
  const parentElement = element.parentElement;

  if (!parentElement) {
    return null;
  }

  if (parentElement.tagName === parentTagName) {
    return parentElement;
  }

  return findParentElement(parentElement, parentTagName);
};

export const withinRange = (minValue, value, maxValue) =>
  Math.min(Math.max(minValue, value), maxValue);

export const keepWithinBounds = (bounds, target) => {
  assert(typeof bounds === "object", "Missing argument `bounds`");
  assert(typeof bounds.x === "number", "Missing argument `bounds.x`");
  assert(typeof bounds.y === "number", "Missing argument `bounds.y`");
  assert(typeof bounds.height === "number", "Missing argument `bounds.height`");
  assert(typeof bounds.width === "number", "Missing argument `bounds.width`");
  assert(typeof target === "object", "Missing argument `target`");
  assert(typeof target.x === "number", "Missing argument `target.x`");
  assert(typeof target.y === "number", "Missing argument `target.y`");
  assert(typeof target.height === "number", "Missing argument `target.height`");
  assert(typeof target.width === "number", "Missing argument `target.width`");

  const top = bounds.y,
    left = bounds.x,
    right = bounds.x + bounds.width - target.width,
    bottom = bounds.y + bounds.height,
    x = withinRange(left, target.x, right),
    y = withinRange(top, target.y, bottom),
    height = bounds.height,
    width = bounds.width;

  return { x, y, height, width, left, top, right, bottom };
};

export const keepWithinSVGBounds = ({ x, y }, element, boundingElement) => {
  if (!element) {
    return { x, y };
  }

  const elementBounds = element.getBoundingClientRect(),
    boundingElementBounds = boundingElement
      ? boundingElement.getBoundingClientRect()
      : elementBounds;

  boundingElementBounds.x = x;

  return keepWithinBounds(boundingElementBounds, {
    x: boundingElementBounds.x,
    y,
    width: element.offsetWidth,
    height: element.offsetHeight,
  });
};
