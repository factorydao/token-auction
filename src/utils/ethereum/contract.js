import { Contract, providers } from "ethers";
import { ethInstance } from "evm-chain-scripts";
import Events from "servment";
import Auction from "truffle/Auction.json";
import { purchaseEventParser } from "utils/curve";
import networks from "../networks.json";

let readContract;
let servmentInstance;
const SUBSCRIBER_SERVICE_URL =
  process.env.SUBSCRIBER_SERVICE_URL || "subscriber-test.influencebackend.xyz";
const SUBSCRIBER_SERVICE_PROTOCOL =
  process.env.SUBSCRIBER_SERVICE_PROTOCOL || "https";
const listeners = {},
  EVENTS = {
    NETWORK_CHANGE: "networkChange",
  };

export function dispatchEvent(eventName, ...args) {
  const handlers = listeners[eventName];

  if (handlers) {
    handlers.forEach((handler) => handler(...args));
  }
}

export function addEventListener(eventName, handler) {
  let handlers = listeners[eventName];

  if (!handlers) {
    handlers = listeners[eventName] = [];
  }

  handlers.push(handler);
}

export function removeEventListener(eventName, handler) {
  const handlers = listeners[eventName];

  if (handlers) {
    const index = handlers.indexOf(handler);

    handlers.splice(index, 1);
  }
}

export const events = {
  EVENTS,
  addEventListener,
  removeEventListener,
};

export const getAccounts = async () => await ethInstance.getAccounts();

export async function metamaskWrongNetwork() {
  const chainId = await ethInstance.getWalletChainId();
  return !!(chainId === 1);
}

export async function getReadContract(contractAbi) {
  if (!readContract) {
    const networkId = await ethInstance.getWalletChainId(),
      contract = contractAbi.networks[networkId];
    if (!contract) {
      throw new Error(`Failed to find network with ID ${networkId}`);
    }

    readContract = await ethInstance.getReadContractByAddress(
      contractAbi,
      contract,
      networkId
    );
  }
  return readContract;
}

export async function getAllEvents(
  callback,
  contractAbi,
  eventName,
  fromBlock = 0,
  toBlock = "latest"
) {
  const chainId = await ethInstance.getWalletChainId();
  const provider = new providers.JsonRpcProvider(networks[chainId].rpc[0]);
  const contract = new Contract(
    contractAbi.networks[chainId].address,
    contractAbi.abi,
    provider
  );
  const contractFilter = contract.filters[eventName]();
  const events = await contract.queryFilter(contractFilter, fromBlock, toBlock);
  ethInstance.subscribeLogEvent(contract, eventName, callback);
  for (const event of events) callback(event);
}
export async function getAllEventsV2(contractAbi, eventName) {
  const servmentInstance = getServmentInstance();
  const chainId = await ethInstance.getWalletChainId();
  const fromBlock = contractAbi.networks[chainId].fromBlock ?? 0;
  const response = await servmentInstance.getAllEvents({
    project: contractAbi.contractName,
    chainId,
    eventName,
    requestBody: {
      abi: servmentInstance.getEventsFromAbi(contractAbi),
      params: {
        contractAddress: Auction.networks[chainId]?.address,
        fromBlock,
        query: {
          fromBlock,
        },
      },
    },
  });
  return response.allEvents.map((event) => purchaseEventParser(event));
}

export async function subscribeToPurchaseEvent(cb) {
  const chainId = await ethInstance.getWalletChainId();
  const provider = new providers.JsonRpcProvider(networks[chainId].rpc[0]);
  const contract = new Contract(
    Auction.networks[chainId].address,
    Auction.abi,
    provider
  );
  ethInstance.subscribeLogEvent(contract, "PurchaseOccurred", cb);
}

export async function getChainHeight() {
  return await ethInstance.getBlock();
}
function getServmentInstance() {
  if (!servmentInstance) {
    servmentInstance = new Events(
      SUBSCRIBER_SERVICE_URL,
      SUBSCRIBER_SERVICE_PROTOCOL
    );
  }
  return servmentInstance;
}
