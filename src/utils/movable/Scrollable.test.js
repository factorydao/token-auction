import React from "react";
import renderer from "react-test-renderer";
import Scrollable from "./Scrollable";

test("should match snapshot", () => {
  const tree = renderer.create(<Scrollable />).toJSON();

  expect(tree).toMatchSnapshot();
});
