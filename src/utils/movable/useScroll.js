import { useState, useEffect } from "react";
import { haltEvent } from "utils/events";

const useScroll = (onScroll, options = {}) => {
  const { propagateEvent } = options;
  const [element, setElement] = useState();

  useEffect(() => {
    function handlMouseWheel(event) {
      const { deltaX, deltaY } = event;

      if (!propagateEvent) {
        haltEvent(event);
      }

      onScroll && onScroll(deltaX, deltaY, event);
    }

    if (element) {
      element.addEventListener("wheel", handlMouseWheel);
    }

    return () => {
      if (element) {
        element.removeEventListener("wheel", handlMouseWheel);
      }
    };
  }, [propagateEvent, element, onScroll]);

  return [setElement];
};
export default useScroll;
