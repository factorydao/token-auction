import { useState, useEffect, useRef, useCallback } from "react";
import { eventToPosition, haltEvent } from "utils/events";

const usePan = (onPan) => {
  const position = useRef({ x: 0, y: 0 }),
    [element, setElement] = useState(null),
    setCurrentPosition = useCallback(
      (x, y) => {
        position.current = { x, y };
      },
      [position]
    ),
    pan = useCallback(
      (x, y) => {
        const { current } = position,
          offsetX = x - current.x,
          offsetY = y - current.y;

        onPan && onPan(offsetX, offsetY);
      },
      [position, onPan]
    ),
    touchDown = useCallback(
      (event) => {
        const { x, y } = eventToPosition(event);

        setCurrentPosition(x, y);
      },
      [setCurrentPosition]
    ),
    takeOff = useCallback(
      (event) => {
        const { x, y } = eventToPosition(event);

        setCurrentPosition(x, y);
      },
      [setCurrentPosition]
    ),
    move = useCallback(
      (event) => {
        const { x, y } = eventToPosition(event);

        pan(x, y);
      },
      [pan]
    );

  useEffect(() => {
    function handleMouseMove(event) {
      haltEvent(event);
      move(event);
    }

    function handleMouseDown(event) {
      haltEvent(event);
      touchDown(event);

      document.addEventListener("mousemove", handleMouseMove);
      document.addEventListener("mouseup", handleMouseUp);
      document.addEventListener("mouseleave", handleMouseUp);
    }

    function handleMouseUp(event) {
      haltEvent(event);
      takeOff(event);

      document.removeEventListener("mousemove", handleMouseMove);
      document.removeEventListener("mouseup", handleMouseUp);
      document.removeEventListener("mouseleave", handleMouseUp);
    }

    function handlTouchMove(event) {
      haltEvent(event);
      move(event);
    }

    function handleTouchStart(event) {
      haltEvent(event);
      touchDown(event);

      document.addEventListener("touchmove", handlTouchMove);
      document.addEventListener("touchend", handlTouchEnd);
    }

    function handlTouchEnd(event) {
      const { current } = position;

      haltEvent(event);
      takeOff({ pageX: current.x, pageY: current.y });

      document.removeEventListener("touchmove", handlTouchMove);
      document.removeEventListener("touchend", handlTouchEnd);
    }

    if (element) {
      element.addEventListener("mousedown", handleMouseDown);
      element.addEventListener("touchstart", handleTouchStart);
    }

    return () => {
      if (element) {
        element.removeEventListener("mousedown", handleMouseDown);
        element.removeEventListener("touchstart", handleTouchStart);
      }
    };
  }, [element, move, takeOff, touchDown]);

  return [setElement];
};
export default usePan;
