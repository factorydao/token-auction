import React, { memo } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import "./Scrollable.scss";

function Scrollable(props) {
  return (
    <div {...props} className={classnames("scrollable", props.className)} />
  );
}

Scrollable.propTypes = {
  className: PropTypes.string,
};

export default memo(Scrollable);
