import { useQuery } from "react-query";
import { getIPInfo } from "net/ip";

const BANNED_COUNTRY_CODES = [
  "US", // United States
  //  'CA', // Canada
  "EG", // Egypt
  "KP", // North Korea
  //  'ID', // Indonesia
  //  'UK', // Britbongistan
  "AE", // United Arab Emirates
  // 'IN', // India
  "CN", // China
  "EC", // Ecuador
  "PK", // Pakistan
  "BD", // Bangladesh
  "SA", // Saudi Arabia
  "BO", // Bolivia
  "IS", // Iceland
  //  'TH', // Thailand
  //  'VN', // Vietnam
  //  'TW', // Taiwan
  //  'RU', // Russia
  "MK", // Macedonia
  "AF", // Afghanistan
  "QA", // Qatar
  "VU", // Vanuatu
  "MA", // Morocco
  "ZM", // Zambia
  "NP", // Nepal
  "AS", // American Samoa
];

export const useIPCheck = () =>
  useQuery("checkIP", () =>
    getIPInfo().then(({ data: { countryCode, proxy } }) => ({
      countryCode,
      banned: BANNED_COUNTRY_CODES.includes(countryCode) || proxy,
    }))
  );
