import React, { memo } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { NavLink } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Menu from "app/menu";
import Logo from "components/logos/AuctionVoteLogo";
import "./Header.scss";

function Header({ className }) {
  return (
    <header className={classnames("header", className)}>
      <Container fluid>
        <Row>
          <Col className="text-left">
            <NavLink to="/">
              <Logo />
            </NavLink>
          </Col>
          <Col className="text-right">
            <Menu className="header__menu" />
          </Col>
        </Row>
      </Container>
    </header>
  );
}

Header.propTypes = {
  className: PropTypes.string,
};

export default memo(Header);
