import { ethInstance } from "evm-chain-scripts";
import { useEffect } from "react";
import { createGlobalState } from "react-hooks-global-state";

const initialState = {
  currentNetwork: process.env.REACT_APP_DEFAULT_CHAIN_ID || 1,
  address: "",
};
const checkNetwork = async (whichTry = 0) => {
  try {
    if (whichTry > 10) return;
    const currentNetwork = await ethInstance.getWalletChainId();
    if (!currentNetwork) {
      return setTimeout(async () => await checkNetwork(whichTry + 1), 300);
    }
    setGlobalState("currentNetwork", currentNetwork);
  } catch (err) {
    console.log("Check network error:", err);
    return err;
  }
};
export const { useGlobalStateProvider, useGlobalState, setGlobalState } =
  createGlobalState(initialState);

export const GlobalStateProvider = ({ children }) => {
  useEffect(() => {
    (async () => await checkNetwork())();
  }, []);

  return children;
};

export default GlobalStateProvider;
